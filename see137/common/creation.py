import copy
import random
import asyncio
import uuid
from concurrent.futures import ThreadPoolExecutor
from typing import AnyStr, Optional, Any, Dict

import dask
import maya
import numpy as np

from jamboree import Jamboree
from jamboree.handlers.default.data import DataHandler
from loguru import logger
from see137 import PortfolioHandler, RequirementsHandler
from see137.data import PriceGenerator
from see137.utils.trades.trade import DynamicTrade, Trade, TradeType
from see137.components.acting.allocs import ActorManager
from see137.handlers import states

# from darwin_ml import MainStateSpace

logger.enable("jamboree")
# logger.enable("see137")
# # logger.disable("see137.handlers.exchange")
# # logger.disable("see137.handlers.allocation")
# logger.disable("see137.utils.ordering")
# # logger.enable("see137.handlers.exchange:save_generated")
# logger.disable('darwin_ml')


class SimulationCreator(object):
    """ 
        # Simulation Creator
        ---
        Create a simulation that we'll be able to run through the faust pipeline.

        * Requirements
        * Stochastic Data
        * Portfolios
        * TimeHandler
    """

    def __init__(
        self,
        start_balance=10000,
        session_id: str = uuid.uuid4().hex,
        ecount=10,
        exchange="fake_exchange",
        **kwargs
    ) -> None:
        self.session: str = session_id
        self.episode_count = ecount
        self.exchange = exchange
        self.start_balance = start_balance
        self.assets = [
            "USD_BTC",
            "USD_ATL",
            "USD_TRX",
            "USD_ETH",
            "USD_BCH",
            "USD_XRP",
            "USD_EOS",
            "USD_XTZ",
            "USD_LINK",
            "USD_ADA"
        ]

        # Generated hexes
        self.episodes = [uuid.uuid4().hex for i in range(self.episode_count)]
        self.user_id = uuid.uuid4(
        ).hex                      # We're generating a fake user id for the time being
                                   #
        self._current_episode: Optional[str] = None
        self.processor = Jamboree()

        # Kwargs
        self.min_price = kwargs.get("min_price", 200)
        self.max_price = kwargs.get("max_price", 5000)
        self.varied = kwargs.get("is_varied", True)
        self.length = kwargs.get("length", 5000)

        # Blanks
        self.generated = None
        self.is_requirements_reset = False

        self.submetatype = "temporary_price"
        self.abbreviation = "GEN"
        # self.__state_placeholder = MainStateSpace()
        self.asset = ""
        self.auto_reset_portfolio = False
        self._data_handler = DataHandler()
        self._portfolio = PortfolioHandler(start_balance=self.start_balance)
        self._requirements = RequirementsHandler()
        self._state = states.StateEnv(prediction_lag=3, window_size=8)

    @property
    def current_episode(self) -> str:
        if self._current_episode is None:
            return uuid.uuid4().hex
        return self._current_episode

    @current_episode.setter
    def current_episode(self, _episode: str):
        self._current_episode = _episode

    @property
    def pricing(self):
        self._data_handler.processor = self.processor
        self._data_handler.category = self.category
        self._data_handler.subcategories = self.subcategories
        self._data_handler.submetatype = self.submetatype
        self._data_handler.abbreviation = self.abbreviation
        return self._data_handler

    @property
    def statespace(self):
        return "<StateSpace message='Calling'>"

    @property
    def category(self):
        return "markets"

    @property
    def subcategories(self):
        return {
            "market": "stock",
            "country": "US",
            "sector": "faaaaake",
            "episode": self.current_episode
        }

    @property
    def requirement_subcategory(self) -> dict:
        return {
            "market": "stock", "country": "US", "sector": "faaaaake"
        }

    @property
    def required(self):
        sub = self.requirement_subcategory
        base = {
            "category": self.category,
            "subcategories": sub,
            "submetatype": self.submetatype,
            "abbreviation": self.abbreviation
        }
        ret_items = []
        for asset in self.assets:
            base = copy.copy(base)
            base['name'] = asset
            ret_items.append(base)
        return ret_items

    @property
    def active(self):
        """ Get all of the active requirements"""
        return self.requirements.assets

    @property
    def sampled_portfolio(self):
        """ Return a sample portfolio for the user """
        assest_num = len(self.assets)
        num_sample = random.randint(2, assest_num)
        asset_sample = random.sample(self.assets, num_sample)
        return asset_sample

    @property
    def generator(self):
        """ Price generator """
        price_generator = PriceGenerator()
        price_generator.episodes = self.episodes
        price_generator.assets = self.assets
        price_generator.starting_min = self.min_price
        price_generator.starting_max = self.max_price
        price_generator.is_varied = self.varied
        price_generator.length = self.length
        return price_generator

    @property
    def portfolio(self):
        """ """
        # portfolio = PortfolioHandler(start_balance=self.start_balance)
        self._portfolio.processor = self.processor
        self._portfolio['session'] = self.session
        self._portfolio['exchange'] = self.exchange
        self._portfolio['user_id'] = self.user_id
        self._portfolio['episode'] = self.current_episode
        self._portfolio['live'] = False
        if self.auto_reset_portfolio:
            self._portfolio.reset()
        self._portfolio.is_sim = True
        self._portfolio.is_inner_allocation = False
        return self._portfolio

    @property
    def state(self) -> states.StateEnv:
        self._state.processor = self.processor
        self._state.exchange = self.exchange
        self._state.user_id = self.user_id
        self._state.live = False
        return self._state

    @property
    def current_generated(self) -> dict:
        """ Get all of the prices for current episode """
        if self.generated is None:
            self.generator.generate_all_episodes()
        current_prices = self.generated
        return current_prices.get(self.current_episode, {})

    @property
    def requirements(self):

        self._requirements.processor = self.processor
        self._requirements['name'] = f"general-{self.session}"
        self._requirements.episode = self.current_episode
        self._requirements.live = False
        self._requirements.reset()
        return self._requirements

    def generate(self):
        self.generated = self.generator.generate_all_episodes()

    def save_generated(self):
        """ Saving generated data for use in real life """
        logger.info("Saving generated data")
        current_pricing = self.pricing
        current_generated = self.current_generated
        for asset in self.assets:
            current_pricing['name'] = asset
            data = current_generated.get(asset)
            current_pricing.reset()
            current_pricing.store_time_df(data, is_bar=True)

    def load_requirements(self):
        required_items = self.required
        self.requirements.asset_update(required_items)
        # self.requirements.update()

    def load_assets(self):
        """ Loads the user's assets into the portfolio """
        user_sample = self.sampled_portfolio
        current_portfolio = self.portfolio
        current_portfolio.reset()
        for asset in user_sample:
            extras = {
                "submetatype": self.submetatype,
                "abbreviation": self.abbreviation
            }
            current_portfolio.add_asset(
                asset, self.subcategories, extras=extras
            )

    def load_unique_requirements(self):
        self.requirements.live = False
        self.requirements.episode = self.current_episode
        self.requirements.unique()

    def load_allocation(self):
        current_portfolio = copy.copy(self.portfolio)
        current_portfolio.step_price(forced=True)

    def adjust_time(self):
        """
            portfolio_hand.time.head = maya.now().add(weeks=25)._epoch
            portfolio_hand.time.change_stepsize(microseconds=0, days=1, hours=0)
            portfolio_hand.time.change_lookback(microseconds=0, weeks=25, hours=0)
        """
        set_time = maya.now().subtract(days=1920)._epoch
        self.portfolio.time.reset()
        self.portfolio.time.head = set_time
        self.portfolio.time.change_stepsize(microseconds=0, days=1, hours=0)
        self.portfolio.time.change_lookback(microseconds=0, weeks=5, hours=0)
        self.portfolio.reset()
        self.portfolio.pricing.sync()

    def start_debug(self):
        self.load_requirements()

    def requirements_params(self) -> dict:
        return {
            'name': f"general-{self.session}",
            'episode': self.current_episode,
            "live": False
        }

    def statespace_params(self) -> Dict[AnyStr, Any]:
        current_reqs = self.requirements.unique()
        assets = current_reqs.assets
        chosen_asset = random.choice(assets)
        asset_name = chosen_asset.get('name')
        return {
            "exchange": self.exchange,
            "user_id": self.user_id,
            "live": False,
            "name": asset_name,
            "asset_dict": chosen_asset,
            "category": self.category,
            "subcategories": self.requirement_subcategory,
            "submetatype": self.submetatype,
            "abbreviation": self.abbreviation,
            "episode": self.current_episode,
            "is_mock_portfolio": False
        }

    def portfolio_params(self):
        return {
            "session": self.session,
            "exchange": self.exchange,
            "user_id": self.user_id,
            "episode": self.current_episode,
            "live": False,
            "is_sim": True,
            "is_inner_allocation": False,
        }

    async def async_porfolio_params(self):
        return self.portfolio_params()

    async def async_statespace_params(self):
        return self.statespace_params()

    async def async_requirements_params(self) -> dict:
        return self.requirements_params()

    async def async_time_step(self):
        self.portfolio.time.step()

    def start(self):
        """ Create all of the episodes"""
        self.load_requirements()
        self.generate()
        logger.info("Successfully starting session")
        generation_tasks = []
        for episode in self.episodes:
            self.current_episode = episode
            self.adjust_time()
            self.save_generated()
            self.load_assets()
            self.load_allocation()
            self.load_unique_requirements()
        self.auto_reset_portfolio = True


def process_rebalance(
    current_portfolio: PortfolioHandler,
    current_requirements: RequirementsHandler
):
    logger.debug("Stepping forward for the user")
    current_portfolio.time.step()
    current_portfolio.is_inner_allocation = True
    current_portfolio.step_price()
    current_portfolio.is_inner_allocation = False
    logger.error("Finished all assets in episode")


if __name__ == "__main__":
    simulation_creator = SimulationCreator(length=2000, ecount=2)
    simulation_creator.start()
    # executor = ThreadPoolExecutor()

    # Get all of the active assets in the simulation

    while True:
        is_done = False
        for episode in simulation_creator.episodes:
            simulation_creator.current_episode = episode
            current_portfolio = simulation_creator.portfolio
            current_requirements = simulation_creator.requirements.unique()
            active_assets = current_requirements.assets
            current_active = copy.copy(active_assets)
            # logger.info(current_active)
            while True:
                if not current_portfolio.pricing.is_next:
                    is_done = True
                    break

                active = current_active.pop()
                symbol = active.get('name')
                asset_msg = f"Processing the asset: {symbol} for episode: {episode} we'd pull data and process it."
                current_item = simulation_creator.pricing
                current_item['name'] = symbol
                current_item.episode = episode
                current_item.live = False
                current_item.reset()

                _subcats = simulation_creator.subcategories
                _subcats.get("episode", None)
                _subcats['user_id'] = simulation_creator.user_id

                statespace = simulation_creator.state
                statespace.name = symbol
                statespace.asset_dict = active
                statespace.category = simulation_creator.category
                statespace.subcategories = _subcats
                statespace.episode = episode
                statespace.submetatype = simulation_creator.submetatype
                statespace.abbreviation = simulation_creator.abbreviation
                statespace.live = False

                statespace.step()
                logger.info(statespace._query)

                user_id = current_portfolio.user_id
                current_portfolio.add_asset(symbol)
                (current_item.closest_head())
                current_allocation = (
                    current_portfolio.current_allocation(symbol=symbol)
                )
                current_limit = (
                    current_portfolio.current_pct_limit(symbol=symbol)
                )
                simulation_creator.asset = current_item.metaid

                simulation_creator.current_episode = episode

                if current_portfolio.is_user_asset(active):
                    if random.uniform(0, 1) < 0.9:
                        trade_type: Optional[TradeType] = None
                        if random.uniform(0, 1) < 0.8:
                            trade_type = TradeType.LIMIT_BUY
                        else:
                            trade_type = TradeType.LIMIT_SELL
                        # Makes trade for portfolio
                        dynamic_trade = DynamicTrade(symbol, trade_type)
                        dynamic_trade.percentage = random.normalvariate(
                            0.7, 0.06
                        )
                        current_portfolio.step(dynamic_trade)
                    else:
                        current_portfolio.step()
                current_requirements.report_cardinality(active)
                if current_requirements.is_valid_cardinality:
                    current_requirements.flush_cardinality()
                    current_portfolio.time.step()

                    logger.error("Next time step in episode")
                    # This should be done elsewhere to prevent locking
                    current_portfolio.step_price(forced=True)
                    break

        if is_done is True:
            break

if __name__ == "__main__":
    import ray
    ray.init()
