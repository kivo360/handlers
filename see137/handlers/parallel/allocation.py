from multiprocessing import cpu_count

import pandas as pd
import ray
from jamboree import Jamboree
from jamboree.handlers import TimeHandler
from ray.util import ActorPool

from see137.handlers.allocation import AllocationHandler as _AllocationHandler
from see137.components.acting.allocs import ActorManager
CPU_NUM = cpu_count()


@ray.remote
class AllocationActor(object):
    def __init__(self):
        self.processor = Jamboree()
        self.allocator = _AllocationHandler()
        self.timer = TimeHandler()
        self.allocator.processor = self.processor
        self.timer.processor = self.processor

    def set_time(self, episode:str, live:bool):
        self.timer['episode'] = episode
        self.timer['live'] = live

    def allocate_remote_and_finish(self, rebalance_frame:pd.DataFrame, user_id:str, exchange:str, episode:str, live:bool=False):
        self.set_time(episode, live)
        self.allocator["episode"] = episode
        self.allocator["user_id"] = user_id
        self.allocator["exchange"] = exchange
        self.allocator["live"] = live
        self.allocator.time = self.timer
        self.allocator.reset()
        
        self.allocator._allocation_and_finish(rebalance_frame)




ActorManager()

class AllocationHandler(_AllocationHandler):
    # pass
    def _allocation_and_finish(self, rebal_frame, is_actor=True):
        if is_actor:
            user_id  = self['user_id']
            exchange = self['exchange']
            episode  = self['episode']
            live     = self['live']
            actor_manager = ActorManager()
            actor_manager.init_actors()
            actor_yay = actor_manager.get_actors()
            actor_yay.allocate_remote_and_finish.remote(rebal_frame, user_id, exchange, episode, live)
        else:
            super()._allocation_and_finish(rebal_frame)
