import socket
import uuid
import numpy as np
from numpy.lib.npyio import load
import pandas as pd
from typing import Dict, Optional, Union, Any, AnyStr

import ray
from jamboree import Jamboree
from loguru import logger
from contextlib import closing

from see137.handlers.detached_actors import DetachedActorHandler
from see137.handlers.exchange import AsyncPortfolioHandler
from see137.handlers.requirements import AsyncRequirements
from see137.handlers.states import AsyncStateEnv, ResponseType
from see137.utils.trades.trade import Trade


class RayCommands:

    def get_node_ip(self):
        """Returns the IP address of the current node."""
        return ray.services.get_node_ip_address()

    def find_free_port(self):
        """Finds a free port on the current node."""
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
            s.bind(("", 0))
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            return s.getsockname()[1]


class LoadBalancer(RayCommands):

    def __init__(
        self,
        host: str = "localhost",
        port: int = 6379,
        _salt: Optional[str] = None,
        _hash: Optional[str] = None
    ) -> None:
        self._salt = _salt or uuid.uuid4().hex
        self._hash = _hash or uuid.uuid4().hex
        self.port = port
        self.host = host
        self._inner_detached = DetachedActorHandler()
        self._inner_detached.processor = Jamboree(
            REDIS_HOST=self.host,
            REDIS_PORT=self.port,
        )
        self._inner_detached.set_keys(self._salt, self._hash)
        # Number of actors per core
        self.resource_num: Dict[str, int] = {
            "exchange": 2,
            "state_space": 1,
            "requirements": 1,
        }
        self.async_classes: Dict[str, int] = {
            "exchange": AsyncPortfolioHandler,
            "state_space": AsyncStateEnv,
            "requirements": AsyncRequirements
        }
        self.cpu_resources: int = ray.cluster_resources().get("CPU", 0)

    async def step(self, name: str, params: dict = {}, *args, **kwargs):
        if name not in self.async_classes:
            raise ValueError("The actor you seek doesn't exist.")
        load_actor = await self.async_choice_by_name(name)
        response = await load_actor.step.remote(*args, **params)
        return response

    async def is_next(self, params: dict = {}):
        """Is Step Complete

        Checks to see if the episode step is complete.

        Parameters
        ----------
        params : dict, optional
            [description], by default {}
        """
        load_actor = await self.async_choice_by_name("requirements")
        response = await load_actor.is_next.remote(**params)
        return response

    async def flush(self, params: dict = {}):
        """Is Step Complete

        Checks to see if the episode step is complete.

        Parameters
        ----------
        params : dict, optional
            [description], by default {}
        """
        load_actor = await self.async_choice_by_name("requirements")
        response = await load_actor.flush.remote(**params)
        return response

    async def step_log_action(self, action, params: dict = {}):
        load_actor = await self.async_choice_by_name("state_space")
        await load_actor.log_action.remote(action, **params)

    async def step_log_returns(self, returns: float, params: dict = {}):
        load_actor = await self.async_choice_by_name("state_space")
        await load_actor.log_returns.remote(returns, **params)

    async def step_log_prediction(self, prediction: float, params: dict = {}):
        load_actor = await self.async_choice_by_name("state_space")
        await load_actor.log_prediction.remote(prediction, **params)

    async def async_choice_by_name(self, name: str):
        load_actor = self._inner_detached.choice_by_name(name)
        return load_actor

    async def start(self):
        for k, v in self.resource_num.items():
            _total_resources = v * self.cpu_resources
            while self._inner_detached.count_actors(k) < _total_resources:
                # NOTE: Intentionally computationally expensive.
                ccls = self.async_classes[k]
                self._inner_detached.count_actors(k)
                self._inner_detached.new_actor(
                    actor_class=ccls,
                    name=k,
                    port=self.port,
                    host=self.host,
                )
        return f"Starting with the index of {self.cpu_resources}"


class DetachedLoadBalancer(DetachedActorHandler):

    def __init__(self) -> None:
        super().__init__()
        self.actor_name = "load_balancer"
        self.params = {}

    def set_params(self, _params: Dict[AnyStr, Any] = {}):
        self.params = _params

    async def start(self):
        # Check to see if there's one of teh
        logger.warning(
            "Starting the load balancer. We won't start it if it already exist in the session."
        )
        self.new_actor(
            LoadBalancer,
            self.actor_name,
            _salt=self.saltkey,
            _hash=self.hashkey
        )
        load_actor = self.choice_by_name(self.actor_name)
        await load_actor.start.remote()

    async def exchange_step(
        self, trade: Optional[Trade] = None, parameters: dict = {}
    ):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        name = "exchange"
        _access_exchange = await load_actor.step.remote(name, params, trade)
        logger.warning(_access_exchange)

    async def allocation_step(
        self, parameters: dict = {}, trade=None, **kwargs
    ):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        params['allocation_only'] = True
        name = "exchange"

        _access_exchange = await load_actor.step.remote(name, params, trade)

        logger.warning(_access_exchange)

    #
    async def state_space_step(
        self,
        response: ResponseType = ResponseType.PREDICT,
        parameters: dict = {},
    ):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        # Paramters should be set dynamically for the given episode and episode tags
        params = parameters
        name = "state_space"
        response = await load_actor.step.remote(name, params, response)
        return response

    async def report(self, asset: Dict[str, Any], parameters: Dict[str, Any]):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        # Paramters should be set dynamically for the given episode and episode tags
        params = parameters
        name = "requirements"
        response = await load_actor.step.remote(name, params, asset)
        return response

    async def is_next(self, parameters: Dict[str, Any]):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        response = await load_actor.is_next.remote(params)
        return response

    async def flush(self, parameters: Dict[str, Any]):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        response = await load_actor.flush.remote(params)
        return response

    async def step_log_action(self, action, parameters: Dict[str, Any]):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        response = await load_actor.step_log_action.remote(action, params)
        return response

    async def step_log_returns(
        self, returns: float, parameters: Dict[str, Any]
    ):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        response = await load_actor.step_log_returns.remote(returns, params)
        return response

    async def step_log_prediction(
        self, prediction: float, parameters: Dict[str, Any]
    ):
        load_actor = self.choice_by_name(self.actor_name)
        if load_actor is None: return

        params = parameters
        response = await load_actor.step_log_prediction.remote(
            prediction, params
        )
        return response
