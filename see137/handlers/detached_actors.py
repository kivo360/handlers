from uuid import uuid4
from jamboree import DBHandler, Jamboree
from typing import Any, Dict, List
from loguru import logger
from typing import Optional
import ray
from ray.actor import ActorHandle
import random


class DetachedActorHandler(DBHandler):

    def __init__(self) -> None:
        super().__init__()
        # Locates the detached actors by phrase hash
        self.entity = "detached_actors"
        self.required = {
            "saltkey": str,
            "hashkey": str,
        }
        self.is_event = False

    @property
    def saltkey(self) -> str:
        return self['saltkey']

    @property
    def hashkey(self) -> str:
        return self['hashkey']

    def add_actor(self, _id: str, actor_name: str):
        """Sets Actor Name

        Sets the actor name by id. 

        Parameters
        ----------
        _id : str
            The actor's id.
        actor_name : str
            The name of the actor we're storing. This id should be standard.
        """
        alt = {
            "detail": "actor_id", "actor_name": actor_name
        }
        value = {
            "identity": _id
        }
        self.save(value, alt=alt)
        self.add_name(actor_name)

    def add_name(self, name: str):
        alt = {
            "detail": "actor_names"
        }
        self.save({"name": name}, alt=alt)

    def count_actors(self, actor_name: str) -> int:
        alt = {
            "detail": "actor_id",
            "actor_name": actor_name,
        }
        return self.count(alt=alt)

    def set_keys(self, salt: str, _hash: str):
        self['saltkey'] = salt
        self['hashkey'] = _hash

    def set_quantity(self, name: str, quantity: int):
        """Set Number Of Actors Per Core

        Used to granularly set the number of actors per core. We need this to spin up possibly more actors than the number of cores we have in the cluster. 
        Use when you need to max out the number of cores you have.

        Parameters
        ----------
        name : str
            The name of the actor you intend to start.
        quantity : int
            The number of actors you're trying to harbor per core.
        """
        pass

    def get_actor(self, name: str) -> ActorHandle:
        actor = ray.util.named_actors._get_actor(name)
        return actor

    def new_actor(self, actor_class: object, name: str, *params, **kwargs):
        """New Actor

        Given a class, we create an actor instance with a given name.

        Parameters
        ----------
        actor_class : object
            An actor class
        name : str
            The named classifcation in which we're to add an actor.
        """
        actor_id = uuid4().hex
        _actor_instance = ray.remote(actor_class)
        _actor_instance.options(name=actor_id).remote(*params, **kwargs)
        self.add_actor(actor_id, name)

    def get_classes_by_session(self) -> List[Dict[str, Any]]:
        alt = {
            "detail": "actor_names"
        }
        return self.many(limit=10000, alt=alt)

    def get_all_actors(self, name: str) -> List[Dict[str, Any]]:
        """Get All Actors

        Get all actors within a certain category.

        Parameters
        ----------
        name : str
            The category of actors we're looking for

        Returns
        -------
        List[Dict[str, Any]]
            A list of dictionaries specifying the actors inside of a category. Use to do operations on them.
        """
        alt = {
            "detail": "actor_id", "actor_name": name
        }
        return self.many(limit=10000, alt=alt)

    def kill_all(self):
        """Kill All

        Kills all actors in session. 
        We identify all of the actors in the sessions and kill them in parallel.
        """
        classes = self.get_classes_by_session()

        if not len(classes) > 0:
            return
        for _class in classes:
            cls_name: Optional[str] = _class.get("name", None)
            if cls_name is None: continue
            self.kill_name(cls_name)

    def kill_name(self, name: str):
        # Kill within a specific name
        _actors = self.get_all_actors(name)
        if not len(_actors) > 0:
            return
        for act in _actors:
            _id: Optional[str] = act.get("identity", None)
            if _id is None: continue
            _act_handler = self.get_actor(_id)
            if _act_handler is None: continue
            ray.kill(_act_handler)
            # Deletes the record from the database

    def choice_by_name(self, name: str, depth: int = 1):
        if self.count_actors(name) > 0:
            _actor_info: dict = random.choice(self.get_all_actors(name))
            _id: Optional[str] = _actor_info.get("identity", None)

            if _id is None:
                if depth < 2:
                    return self.choice_by_name(name, depth + 1)
                raise AttributeError(
                    "No actors with that name inside of the database."
                )
                # Keep on trying.
            _actor = self.get_actor(_id)
            if _actor is None:
                raise AttributeError("No actors with that name inside of the database.")        
            return _actor
        raise AttributeError("No actors with that name inside of the database.")
