from loguru import logger
from see137.handlers.models import props


class OnlineTorchContainers(props.OnlineTorchStateWithProperties):
    """Online Torch Containers

    We use the containers to manage the save, load and merge functions for the machine learning system.

    Parameters
    ----------
    props : OnlineTorchStateWithProperties
        These are the key properties we need to keep track of.
    """

    def __init__(self):
        super().__init__()

    def save_model(self):
        """Save Model

        Saves the current model into the background storage space with the given parameters. 
        """
        logger.error(f"Saving model: {self.state_dict}")

    def load_model(self):
        """Load Model

        Loads models using the given parameter. Usually we have a model for a given sesison. They can transfer if we want to use a supplied key.
        """
        logger.success("Loading and setting model parameters")