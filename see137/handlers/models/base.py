from see137.importers import *
from faker import Faker
from jamboree.handlers.complex.backtestable.db import (
    BacktestDBHandlerWithAccess
)
from pydantic import BaseModel, Field

from darwin_ml.handlers import IncrementalEngine

__all__ = ['BaseOnlinePytorch']

fake = Faker()


class SubcategoryDict(BaseModel):
    market: str = Field(default_factory=fake.cryptocurrency_name)


class AssetDict(BaseModel):
    name: str = Field(default_factory=fake.cryptocurrency_name)
    category: str = "markets"
    subcategories: dict = Field(default_factory=SubcategoryDict().dict)
    submetatype: str = "GEN"
    abbreviation: str = Field(default_factory=fake.cryptocurrency_code)
    live: bool = False
    episode: str = Field(default_factory=lambda: uuid.uuid4().hex)


class BaseOnlinePytorch(BacktestDBHandlerWithAccess):
    """
        # Online Learning PyTorch

        Online learning for PyTorch. 

        We do the following:

        1. Load/Store the parameters.
        2. Set the params
        3. Record predictions and recent changes.
        4. Return the states space for immediate consumption.

    """

    def __init__(self):
        super().__init__()
        # Entity Variables
        self.entity = "onlinetorch"
        self.submetatype = "generalizable"
        # This information should be the exact same as the requirements
        self.required = {
            "name": str,
            "session": str,
        }
        self.metatype = self.entity
        self.is_modelset: bool = False
        # We use this.blobfile to store the training object.
        self._incremental_engine: IncrementalEngine = IncrementalEngine()
        # self._incremental_engine.blobfile = self._space

        _sesison = uuid.uuid4().hex

        # Models and shit

        self.__model: Optional[nn.Module] = None
        self.__optimizer: Optional[torch.optim.Optimizer] = None
        self.__loss: Optional[nn.Module] = None

        self.__session: str = _sesison

        self.session = _sesison

        self.autoset_model()

    @property
    def optimizer(self):
        return self.__optimizer

    @optimizer.setter
    def optimizer(self, _optimizer):
        is_exist = (_optimizer is not None)
        is_optimizer = isinstance(_optimizer, torch.optim.Optimizer)
        if not is_optimizer or not is_exist: return
        self.__optimizer = _optimizer

    @property
    def loss(self):
        return self.__loss

    @loss.setter
    def loss(self, _loss):
        is_exist = _loss is not None
        is_optimizer = isinstance(_loss, nn.Module)
        if not is_optimizer or not is_exist: return
        self.__loss = _loss

    @property
    def model(self) -> Optional[nn.Module]:
        """Local ML Model

        Returns the local pytorch model. We'd have something separete for tensorflow.

        Returns
        -------
        Optional[nn.Module]
            The model. Can be None.
        """
        return self.__model

    @model.setter
    def model(self, _model: nn.Module) -> Optional[nn.Module]:
        """(Property) - Set ML Model 
        
        Doesn't allow the model to be stored locally if it's not an instance of nn.Module (a pytorch model).
        """
        if not isinstance(_model, nn.Module): return
        self.__model = _model
        self.autoset_model()

    @property
    def session(self):
        return self.__session

    @session.setter
    def session(self, _session: str):
        self.__session = _session
        self['session'] = _session

    @property
    def name(self) -> str:
        return self['name']

    @name.setter
    def name(self, _name: str):
        self['name'] = _name

    @property
    def current_time(self) -> float:
        return self.time.head

    def autoset_model(self):
        """ Autoset Model

        Automatically sets the model inside of the incremental saving subsystem.
        """
        logger.info("Attempting to set the model in-memory.")
        # Reference here to prevent duplicated checks.
        current_model = self.model
        is_exist = (current_model is not None)
        is_module = isinstance(current_model, nn.Module)

        if not is_exist or not is_module:
            logger.error("Model not set")
            return
        logger.success("Model successfully set locally")
        self.is_modelset: bool = True
        self._incremental_engine.blobfile = current_model
        self.optimizer = torch.optim.SGD(current_model.parameters(), lr=0.15)
        self.loss = nn.CrossEntropyLoss()
