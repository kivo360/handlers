from see137.importers import *
from see137.handlers.models import examples, contain


class OnlinePyTorchMachine(contain.OnlineTorchContainers):
    """Online Pytorch Machine Learning

    This class us used to manage all machine learning operations in the loop. The layer above manages.

    Parameters
    ----------
    contain : contain.OnlineTorchContainers
        The online containers manages all of the saving commands inside of the loop.
    """

    def __init__(self):
        """
            TODO: Add get and set(s) estimate functions. 
            Also get_before() function to better allow for better dataframe alignment
        """

        super().__init__()

    def train(self, batch: torch.Tensor, prediction: torch.Tensor):
        if self.is_modelset is False:
            logger.error("NOT TRAINING")
            return
        local_prediction = self.model(batch)
        self.optimizer.step()
        self.loss(local_prediction.long(), prediction.long())
        self.loss.backward()


def test_inloop():
    online_machine = OnlinePyTorchMachine()
    online_machine.model = examples.ConvNet()
    time.sleep(3)
    is_init = False
    while True:
        if not is_init:
            logger.error("Initialized Model")
            is_init = True

        # Figure out training data of some sort.
        fake_batch = torch.randn((10, 1, 28, 28))
        fake_prediction = torch.randn((10))
        logger.debug(f"Getting training data: {fake_batch}")
        online_machine.train(fake_batch, fake_prediction)
        logger.warning(f"Starting training loop")
        # We can pull a random np.array here.
        online_machine.save_model()
        online_machine.load_model()
        logger.info("Push model parameters into place")


if __name__ == "__main__":
    test_inloop()