import os

import pytest
import torch
import torch.nn as nn
import torch.nn.functional as F
from jamboree import Jamboree
from torchvision import datasets, transforms

from see137 import data
from see137.handlers.states import actions, envs, machine
from see137.importers import *

REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = int(os.getenv("REDIS_PORT", "6379"))


class ConvNet(nn.Module):
    """Small ConvNet for MNIST."""

    def __init__(self):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 3, kernel_size=3)
        self.fc = nn.Linear(192, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 3))
        x = x.view(-1, 192)
        x = self.fc(x)
        return F.log_softmax(x, dim=1)

    def get_weights(self):
        return {k: v.cpu() for k, v in self.state_dict().items()}

    def set_weights(self, weights):
        self.load_state_dict(weights)

    def get_gradients(self):
        grads = []
        for p in self.parameters():
            grad = None if p.grad is None else p.grad.data.cpu().numpy()
            grads.append(grad)
        return grads

    def set_gradients(self, gradients):
        for g, p in zip(gradients, self.parameters()):
            if g is not None:
                p.grad = torch.from_numpy(g)


@pytest.fixture(scope="session")
def jam_proc():
    """Jamboree Processor

    Returns a connected jamboree processor for all tests.

    Returns
    -------
    Jamboree
        Jamboree session
    """
    return Jamboree(REDIS_HOST=REDIS_HOST, REDIS_PORT=REDIS_PORT)


@pytest.fixture(scope="session")
def stp_actions(jam_proc: Jamboree):
    """StateSpace Actions

    All of the primary state space actions. It's used to hold highly specific functions 
    we'll use to shift data around. To be used with the other components (like context and Envs).

    Parameters
    ----------
    jam_proc : Jamboree
        Jamboree fixture

    Returns
    -------
    actions.ActionsStateSpace
        ActionStateSpace
    """
    _actions = actions.ActionsStateSpace()
    _actions.processor = jam_proc
    _actions.init_default()
    return _actions


@pytest.fixture(scope="session")
def pytorch_management(jam_proc: Jamboree):
    """PyTorch Management

    Pytorch model management(er)

    Parameters
    ----------
    jam_proc : Jamboree
        The jamboree processor. The thing we use to store the data and transfer it between machine.

    Returns
    -------
    ???
        ???
    """
    _actions = actions.ActionsStateSpace()
    _actions.processor = jam_proc
    _actions.init_default()
    return _actions


@pytest.fixture(scope="session")
def example_pytorch_model():
    return ConvNet()