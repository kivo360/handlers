from jamboree.handlers.abstracted import datasets
from typing import Any, Dict
from darwin_ml.handlers import (
    IncrementalEngine,
)
from typing import Any, AnyStr

from loguru import logger

from see137.handlers.models import base


class OnlineTorchStateWithProperties(base.BaseOnlinePytorch):

    def __init__(self):
        super().__init__()

    @property
    def incremental(self) -> IncrementalEngine:
        """Stacked State Space

        Return the stacked state space. The stacked state space is n number of steps
        """
        # This could get so fucked IRL. Who cares? Yolo, we're pro-life here ...?
        self._incremental_engine.processor = self.processor
        self._incremental_engine.name = self.name
        self._incremental_engine.category = "torch_incrementals"
        self._incremental_engine.subcategories = {
            "base": "torched"
        }
        self._incremental_engine.abbreviation = "TORCH"
        self._incremental_engine.submetatype = self.submetatype
        self._incremental_engine.live = self.live
        self._incremental_engine.episode = "aint_no_thang"
        return self._incremental_engine

    @property
    def current_time(self) -> float:
        return super().time.head

    @property
    def update_dict(self) -> Dict[str, Any]:
        return {
            "episode": self.episode,
            "live": self.live,
            "time": self.current_time, # "timestamp": self.current_time
        }

    @property
    def state_dict(self) -> Dict[AnyStr, Any]:
        if self.model is None:
            return {}
        # NOTE: to self ... try figuring the state_dict parameters.
        return self.model.state_dict()

    @state_dict.setter
    def state_dict(self, _state_dict: Dict[AnyStr, Any]):

        logger.info(f"Setting state dict: {_state_dict}")
        logger.info(
            f"Testing that the state dict isn't invalid (empty or lacking in a series of criteria): {_state_dict}"
        )
        self.model.load_state_dict(_state_dict)


__all__ = ['OnlineTorchStateWithProperties']
