# import pytest
import random

from darwin_ml.models import StateSpaceModel
from loguru import logger
import numpy as np

from see137.handlers.states import actions

logger.configure(activation=[("see137.handlers.states.actions", True)],)


def test_step_increment(stp_actions: actions.ActionsStateSpace):
    """Test Increment Single Asset

    Test that we can increment both a single asset and the full episode

    Parameters
    ----------
    stp_actions : actions.ActionsStateSpace
        The ActionState we're acting on
    """
    _first = stp_actions.get_step_count(is_asset=True)
    stp_actions.update_step(is_asset=True)
    _second = stp_actions.get_step_count(is_asset=True)
    stp_actions.update_step(is_asset=True)
    _third = stp_actions.get_step_count(is_asset=True)
    stp_actions.update_step(is_asset=True)

    # Each variable should be larger than the last
    assert _first < _second < _third


def test_load_save_file(stp_actions: actions.ActionsStateSpace):
    """Load & Save State Blob

    Loads the state space to test that we can do file manipulation.

    Parameters
    ----------
    stp_actions : actions.ActionsStateSpace
        ActionStateSpace
    """
    # Initialize
    _first = stp_actions.load_state_blob()
    _first_lag = stp_actions.get_prediction_step_lag()
    # Clear
    stp_actions.clear_state_blob()
    _second = stp_actions.pull_model()

    # Load again. Should be the first version
    _third = stp_actions.load_state_blob()
    stp_actions._lag = 4

    stp_actions.save_state_blob(update_lag=True)
    stp_actions.load_state_blob()
    _second_lag = stp_actions.get_prediction_step_lag()

    assert _first is not None
    assert _second is None
    assert _third is not None

    assert isinstance(_first, StateSpaceModel)
    assert isinstance(_third, StateSpaceModel)
    assert _first_lag != _second_lag


def test_state_manipulation(stp_actions: actions.ActionsStateSpace):
    """Test Full State Manipulation

    Test manipulating state space vectors.
    Store and load functions for states, actions and rewards only.
    """
    mock_state_spaces = [np.random.rand(6, 6) for _ in range(4)]
    mock_actions = [
        (random.randint(0, 2), random.uniform(0, 1)) for _ in range(4)
    ]
    mock_rewards = [random.uniform(0, 1) for _ in range(4)]
    stp_actions.time.reset()
    start_time = stp_actions.current_time

    for index in range(4):
        curr_action = mock_actions[index]
        curr_space = mock_state_spaces[index]
        curr_reward = mock_rewards[index]

        stp_actions.save_action(curr_action)
        stp_actions.save_state(curr_space)
        stp_actions.save_reward(curr_reward)
        stp_actions.time.step()

    stp_actions.time.head = start_time
    # NOTE: Put all of this into a single loop to save with time
    first_act = stp_actions.load_action()
    first_state = stp_actions.load_state()
    first_reward = stp_actions.load_reward()
    assert first_act is not None, "We weren't able to get the last action."
    assert first_state is not None, "We weren't able to get the latest state"
    assert first_reward is not None, "We weren't able to get the latest reward"

    assert stp_actions.get_count("actions") is 4, "The number of actions for the currency is not equal to 4"
    assert stp_actions.get_count("state_space") is 4, "The number of states for the currency is not equal to 4"
    assert stp_actions.get_count("reward") is 4, "The number of actions for the currency is not equal to 4"

    for _ in range(4):
        stp_actions.time.step()

    assert first_reward == stp_actions.load_reward(-4), "The 4th reward hasn't been set."
    assert np.array_equal(first_act, stp_actions.load_action(-4)), "The 4th action hasn't been set."
    assert np.array_equal(first_state, stp_actions.load_state(-4)), "The 4th state hasn't been set."

    assert len(stp_actions.load_action_set(-2)) == 2, "We didn't get 2 actions from the current time"
    assert len(stp_actions.load_state_set(-2)) == 2, "We didn't get 2 states from the current time"
    assert len(stp_actions.load_reward_set(-2)) == 2, "Didn't get 2 rewards from current time."