# import pytest
from see137.importers import *
from see137.handlers.states import machine
from darwin_ml.models import statespace
from jamboree.utils.context import timecontext

# "see137.handlers.states.contain", False)
#
logger.configure(
    activation=[
        ('jamboree.utils.context', True),
        ('darwin_ml.technical', False),
        ('see137.handlers.exchange', False),
        ("see137.handlers.states.transforms", False),
    ]
)


def test_access_container(stp_container: machine.StateSpaceMachine):
    """Test Increment Single Asset

    Test that we can increment both a single asset and the full episode

    Parameters
    ----------
    stp_actions : contain.StateSpaceContainers
        The State Space Container we're using.
    """

    stp_container.user_id = uuid.uuid4().hex
    allo_cont = stp_container.allocation_container
    price_cont = stp_container.price_container
    feat_cont = stp_container.feature_container

    assert allo_cont, "AllocationContainer not available"
    assert price_cont, "PriceContainer not available"
    assert feat_cont, "FeatureContainer not available"


def test_model_io(stp_container: machine.StateSpaceMachine):
    """Test Increment Single Asset

    Test that we can increment both a single asset and the full episode

    Parameters
    ----------
    stp_actions : contain.StateSpaceContainers
        The State Space Container we're using.
    """

    first = stp_container._lag
    with stp_container as cont:
        assert cont is not None
        assert isinstance(cont, statespace.StateSpaceModel)
        stp_container._lag = 4
    second = stp_container._lag
    assert first is not second, "The first and the second lags shouldn't be the same."


def test_machine_transform(stp_container: machine.StateSpaceMachine):
    """Test Increment Single Asset

    Test that we can increment both a single asset and the full episode

    Parameters
    ----------
    stp_actions : contain.StateSpaceContainers
        The State Space Container we're using.
    """

    machine_transformation = stp_container.get_transformed()
    assert machine_transformation is not None
    assert isinstance(machine_transformation[0], np.ndarray)

    latest_state = stp_container.get_transform_log()
    assert latest_state is not None, "State not found"
    assert isinstance(latest_state, np.ndarray), "State not saved correctly"