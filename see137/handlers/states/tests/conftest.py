import os

from jamboree import Jamboree
import pytest

from see137.handlers.states import actions, machine, envs
from see137.importers import *
from see137 import data

REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = int(os.getenv("REDIS_PORT", "6379"))


@pytest.fixture(scope="session")
def jam_proc():
    """Jamboree Processor

    Returns a connected jamboree processor for all tests.

    Returns
    -------
    Jamboree
        Jamboree session
    """
    return Jamboree(REDIS_HOST=REDIS_HOST, REDIS_PORT=REDIS_PORT)


@pytest.fixture(scope="session")
def stp_actions(jam_proc: Jamboree):
    """StateSpace Actions

    All of the primary state space actions. It's used to hold highly specific functions 
    we'll use to shift data around. To be used with the other components (like context and Envs).

    Parameters
    ----------
    jam_proc : Jamboree
        Jamboree fixture

    Returns
    -------
    actions.ActionsStateSpace
        ActionStateSpace
    """
    _actions = actions.ActionsStateSpace()
    _actions.processor = jam_proc
    _actions.init_default()
    return _actions


@pytest.fixture(scope="session")
def stp_container(jam_proc: Jamboree):
    """StateSpace Actions

    All of the primary state space actions. It's used to hold highly specific functions 
    we'll use to shift data around. To be used with the other components (like context and Envs).

    Parameters
    ----------
    jam_proc : Jamboree
        Jamboree fixture

    Returns
    -------
    actions.ActionsStateSpace
        ActionStateSpace
    """
    _actions = machine.StateSpaceMachine()
    _actions.processor = jam_proc
    _actions.init_default()
    _actions.user_id = uuid.uuid4().hex

    price_gen = data.PriceGenerator()
    price_gen.episodes = [_actions.episode]
    price_gen.length = 2000
    price_gen.assets = [_actions.asset_dict.abbreviation]
    generated = price_gen.generate()
    pricing = generated.get(_actions.asset_dict.abbreviation, {})
    _actions.price.store_time_df(pricing, is_bar=True)
    _actions.reset_technical_analysis()
    return _actions


@pytest.fixture(scope="session")
def state_envs(jam_proc: Jamboree):
    """StateSpace Actions

    All of the primary state space actions. It's used to hold highly specific functions 
    we'll use to shift data around. To be used with the other components (like context and Envs).

    Parameters
    ----------
    jam_proc : Jamboree
        Jamboree fixture

    Returns
    -------
    actions.ActionsStateSpace
        ActionStateSpace
    """
    price_gen = data.PriceGenerator()

    episode = uuid.uuid4().hex
    user_id = uuid.uuid4().hex

    _env = envs.StateEnv(window_size=5)
    _env.processor = jam_proc
    _env.init_default()
    _env.user_id = user_id
    _env.episode = episode

    abbv = _env.asset_dict.abbreviation
    episode_list = [episode]
    abbv_list = [abbv]

    # Create a
    price_gen.length = 2000
    price_gen.assets = abbv_list
    price_gen.episodes = episode_list
    generated = price_gen.generate()

    pricing = generated.get(abbv, {})
    _env.price.store_time_df(pricing, is_bar=True)
    _env.is_mock_portfolio = True
    _env.is_mock = True
    _env.reset()
    return _env