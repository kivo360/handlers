# import pytest
from see137.importers import *
from see137.handlers.states import indexing

from darwin_ml.models import statespace

# "see137.handlers.states.contain", False)
#
logger.configure(
    activation=[
        ('darwin_ml.technical', False),
        ('see137.handlers.exchange', False),
        ("see137.handlers.states.transforms", False),
    ]
)


def test_train_indexing():
    """
        Delayed For Time Bound Indexing
        
        ---
        
        First step to supplying a finished state space. It's to ensure we're aligning all of our variables.
    """
    indexer = indexing.Indexer(ahead=3, window_size=4)

    indexer.train()
    state_index = indexer.state
    action_index = indexer.action

    assert state_index.x_index == -3, "The state index wasn't -3. It should be -3 spaces from zero according."
    assert state_index.x_tail == -7, "The state index wasn't -7. It should be -3-4 spaces from zero according to how the logic should go."

    assert action_index.x_index == -3, "The action index wasn't -3. It should be -3 spaces from zero according."
    assert action_index.x_tail == -7, "The action index wasn't -7. It should be -3-4 spaces from zero according to how the logic should go."

    # Ensure the y windows are appropiate.
    # We can adapt the outputs to real indexes afterwards.
    reward_indexes = indexer.reward

    assert reward_indexes.y_index_head == 0, "Y index should be 0"
    assert reward_indexes.y_index_tail == -4, "Y tail index should be the 0 - 4 = -4"


def test_pred_indexing():
    """
        Get Prediction Indexes:
        ---
            1. Know where we're getting x from. The beginning of the window to the end.
            2. Know where we're storing Y. Having the full window would work as well.
        
        First step to supplying a finished state space. It's to ensure we're aligning all of our variables.
    """
    indexer = indexing.Indexer(ahead=3, window_size=4)

    indexer.pred()
    state_index = indexer.state
    reward_indexes = indexer.reward
    action_index = indexer.action

    # These are the variables we'd used to get a prediction from the ML agent.
    assert state_index.x_index == 0, "The state index wasn't 0. It should be 0 according our indexing rules."
    assert state_index.x_tail == -4, "The state index wasn't -4. It should be index - window_size."

    assert action_index.x_index == 0, "The action index wasn't -0. It should be -3 spaces from zero according."
    assert action_index.x_tail == -4, "The action index wasn't -4. It should be -3-4 spaces from zero according to how the logic should go."

    # This is where we're going to store the predictions.
    assert reward_indexes.y_target_head == 3, "Should be index + look_ahead size."
    assert reward_indexes.y_target_tail == -1, "Should be y_target - window_size or 3 - 4 = -1."


def test_loss_indexing():
    """
        Get Loss Indexes:
        ---
            1. Know where we're getting y and y_hat from. The beginning of the windows to the end. Can use to get full mini-batch.        
    """
    indexer = indexing.Indexer(ahead=3, window_size=4)

    indexer.loss()

    reward_indexes = indexer.reward

    # This is where we're going to store the predictions.
    assert reward_indexes.y_index_head == 0, "Should be index alone."
    assert reward_indexes.y_index_tail == -4, "Should be index - window_size or 0 - 4 = -4."

    assert reward_indexes.y_target_head == -3, "Should be index - look_ahead or 0 - 3 = -3."
    assert reward_indexes.y_target_tail == -7, "Should be y_target_head - 4 = -3 -4 = -7."