# import pytest
from see137.handlers.states import envs
from see137.importers import *

# "see137.handlers.states.contain", False)
#
logger.configure(
    activation=[
        ('jamboree.utils.context', True),
        ('darwin_ml.technical', False),
        ('see137.handlers.exchange', False),
        ("see137.handlers.states.transforms", False),
    ]
)


def test_prediction_frame(state_envs: envs.StateEnv):
    """
    Test Prediction Frame
    ---

    Test that we can increment both a single asset and the full episode

    Parameters
    ----------
    stp_actions : contain.StateSpaceContainers
        The State Space Container we're using.
    """

    _min = state_envs.technicals.get_minimum_time()
    state_envs.time.head = _min
    index = 0
    while state_envs.price.is_next:
        _train = state_envs.step_train()
        state_envs.time.step()
        state_envs.step_log_pred(random.uniform(0, 4))
        state_envs.step_log_returns(random.uniform(0, 5))
        state_envs.save_action(np.random.uniform((1, 2)))
        state_envs.step_loss()
        index += 1
        logger.warning(_train)
        if index > 20:
            break

    assert True, "We were not able to create a state space"
