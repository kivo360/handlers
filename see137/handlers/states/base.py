from typing import Any, Dict, Optional
import uuid
import numpy as np
import pandas as pd
from addict import Dict as aDict
from darwin_ml.handlers import (
    IncrementalEngine, TAPredictionTransformer, TAPriceData
)
from darwin_ml.middleware.containers import (
    AllocationContainer, FeaturesContainer, PriceContainer
)

from darwin_ml.models import (
    AllocationStepSystem, OnlineModels, PriceDeltaModel, StateSpaceModel
)
from darwin_ml.models.creme_ml.transforms import (FeatureWindow)
from darwin_ml.technical import (fiboacci_general, fibonacci)
from darwin_ml.utils import (Injectable, InjectableTime)
from jamboree.handlers.abstracted.datasets import PriceData
from jamboree.handlers.complex.backtestable.db import (
    BacktestDBHandlerWithAccess
)

from jamboree.handlers.default import (BlobStorageHandler)
from jamboree.utils.context import timecontext
from loguru import logger
from pydantic import BaseModel

from see137.handlers.exchange import PortfolioHandler
from pydantic import BaseModel, Field
from faker import Faker

__all__ = ['BaseStateSpace']

fake = Faker()


class SubcategoryDict(BaseModel):
    market: str = Field(default_factory=fake.cryptocurrency_name)


class AssetDict(BaseModel):
    name: str = Field(default_factory=fake.cryptocurrency_name)
    category: str = "markets"
    subcategories: dict = Field(default_factory=SubcategoryDict().dict)
    submetatype: str = "GEN"
    abbreviation: str = Field(default_factory=fake.cryptocurrency_code)
    live: bool = False
    episode: str = Field(default_factory=lambda: uuid.uuid4().hex)


class BaseStateSpace(BacktestDBHandlerWithAccess):
    """
        # StateSpace

        This is a direct copy over of the state space.

        The general goal of this class is to ensure we can adapt to the proper elements of the end to end testing suite.

        We do the following:

        1. Load/Store the online learning models
        2. Get the appropiate data to make predictions and changes on.
        3. Record predictions and recent changes.
        4. Return the states space for immediate consumption.

    """

    def __init__(self, lag: int = 3):
        super().__init__()
        # Entity Variables
        self.entity = "statespace"
        self.submetatype = "generalizable"
        self.required = {
            "name": str,
            "category": str,
            "subcategories": dict,
            "metatype": str,
            "submetatype": str,
            "abbreviation": str
        }
        self.metatype = self.entity
        self._lag = lag
        self.__is_mock: bool = False

        # User asset variables
        self.__user: Optional[str] = None
        self.__exchange: str = "binance"
        self.__asset_dict: Optional[AssetDict] = None

        self._incremental_engine: IncrementalEngine = IncrementalEngine()
        self._space: StateSpaceModel = StateSpaceModel(prediction_lag=self._lag)
        self._price: PriceData = PriceData()
        self._technical = TAPriceData()
        self._incremental_engine.blobfile = self._space
        self._portfolio: PortfolioHandler = PortfolioHandler()
        self.__is_mock_portfolio = False

    @property
    def asset_dict(self) -> AssetDict:
        return self.__asset_dict

    @asset_dict.setter
    def asset_dict(self, _asset_dict: Dict[str, Any]):
        self.__asset_dict = AssetDict(**_asset_dict)

    @property
    def current_time(self) -> float:
        return self.time.head

    @property
    def user_id(self) -> str:
        return self.__user

    @user_id.setter
    def user_id(self, user_id: str):
        self.__user = user_id
        self['user_id'] = self.__exchange

    @property
    def exchange(self) -> str:
        return self.__exchange

    @exchange.setter
    def exchange(self, exchange: str):
        self.__exchange = exchange
        self['exchange'] = self.__exchange

    @property
    def is_mock(self) -> bool:
        return self.__is_mock

    @is_mock.setter
    def is_mock(self, __is_mock: bool):
        self.__is_mock = __is_mock

    @property
    def is_mock_portfolio(self) -> bool:
        return self.__is_mock_portfolio

    @is_mock_portfolio.setter
    def is_mock_portfolio(self, __is_mock: bool):
        self.__is_mock_portfolio = __is_mock

    @property
    def episode(self) -> str:
        return self._episode

    @episode.setter
    def episode(self, _episode: str):
        self._episode = _episode
        self['episode'] = _episode

    @property
    def lag(self):
        return self._lag