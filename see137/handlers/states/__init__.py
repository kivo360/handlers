from jamboree import Jamboree
from see137.importers import *
from . import envs
from .envs.pub import StateEnv, ResponseType


class AsyncStateEnv:

    def __init__(self, host: str = "localhost", port: int = 6379) -> None:
        self._state_env = StateEnv()
        self._state_env.processor = Jamboree(REDIS_HOST=host, REDIS_PORT=port)

    def set_parameters(self, **kwargs):
        """     statespace.live = False
                statespace.is_mock = True
                statespace.is_mock_portfolio = True
        """
        user_id = kwargs.get('user_id')
        category = kwargs.get('category')
        subcategories = kwargs.get('subcategories', {})
        abbreviation = kwargs.get('abbreviation')
        episode = kwargs.get('episode')
        asset_dict = kwargs.get('asset_dict', {})
        is_live = kwargs.get('live', False)
        is_mock = kwargs.get('is_mock', True)
        is_mock_portfolio = kwargs.get('is_mock_portfolio', True)
        symbol = asset_dict.get("name")

        user_id = str(user_id)
        category = str(category)
        abbreviation = str(abbreviation)
        episode = str(episode)
        asset_dict = asset_dict
        is_live = is_live
        is_mock = is_mock
        is_mock_portfolio = is_mock_portfolio
        self._state_env.name = str(symbol)
        self._state_env.abbreviation = abbreviation
        self._state_env.category = category
        self._state_env.subcategories = subcategories
        self._state_env.user_id = user_id
        self._state_env.episode = episode
        self._state_env.asset_dict = asset_dict
        self._state_env.live = is_live
        self._state_env.is_mock = is_mock
        self._state_env.is_mock_portfolio = is_mock_portfolio

    def step(
        self,
        response: ResponseType = ResponseType.PREDICT,
        **kwargs
    ) -> np.array:
        self.set_parameters(**kwargs)
        self.reset()
        result = self._state_env.step(response)

        return result

    def log_prediction(self, prediction: float, **kwargs):
        self.set_parameters(**kwargs)
        self.reset()
        self._state_env.step_log_pred(prediction)

    def log_returns(self, returns: float, **kwargs):
        self.set_parameters(**kwargs)
        self.reset()
        self._state_env.step_log_returns(returns)

    def log_action(self, action, **kwargs):
        self.set_parameters(**kwargs)
        self.reset()
        self._state_env.save_action(action)

    def reset(self):
        self._state_env.reset()