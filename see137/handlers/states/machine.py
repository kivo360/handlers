from see137.importers import *
from darwin_ml.models import statespace
from see137.handlers.states import contain


class StateSpaceMachine(contain.StateSpaceContainers):
    """State Space Container

    Properties to get containers representing the current properties of the user price pair.

    Parameters
    ----------
    base : [type]
        [description]
    """

    def __init__(self, prediction_lag: int = 3):
        """
            TODO: Add get and set(s) estimate functions. 
            Also get_before() function to better allow for better dataframe alignment
        """

        super().__init__(prediction_lag)

    def __enter__(self) -> statespace.StateSpaceModel:
        return self.load_state_blob()

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.save_state_blob()
        self.clear_state_blob()

    def get_transformed(self) -> np.ndarray:
        ac = self.allocation_container
        pc = self.price_container
        fc = self.feature_container
        transformed = np.array([])
        # logger.info(ac)
        # logger.warning(fc)
        # logger.error(pc)
        with self as online:
            transformed = online.step(
                all_c=ac,
                feature_container=fc,
                price_container=pc,
            )
        return transformed

    def get_transform_log(self) -> np.ndarray:
        # TODO: Remember to change state save to show more variables
        transformed_np = self.get_transformed()
        if transformed_np is not None:
            super().save_state(transformed_np[0])
            return transformed_np[0]
        return transformed_np

    def state_count(self) -> int:
        return self.get_count("state_space")

    def reward_count(self) -> int:
        return self.get_count("reward")

    def action_count(self) -> int:
        return self.get_count("actions")

    def estimation_count(self) -> int:
        return self.get_count("estimation")