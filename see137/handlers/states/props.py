from jamboree.handlers.abstracted import datasets
from typing import Any, Dict
from darwin_ml.handlers import (
    TAPriceData,
    IncrementalEngine,
)
from see137.handlers.states import (
    base,
)


class StateWithProperties(base.BaseStateSpace):

    def __init__(self, prediction_lag: int = 3):
        super().__init__(prediction_lag)

    @property
    def portfolio(self):
        self._portfolio.processor = self.processor
        self._portfolio.exchange = self.exchange
        self._portfolio.user_id = self.user_id
        self._portfolio.episode = self.episode
        self._portfolio['live'] = self.live
        self._portfolio.reset()
        return self._portfolio

    @property
    def price(self) -> datasets.PriceData:
        self._price.processor = self.processor
        self._price.name = self.asset_dict.name
        self._price.category = self.asset_dict.category
        self._price.subcategories = self.asset_dict.subcategories
        self._price.submetatype = self.asset_dict.submetatype
        self._price.abbreviation = self.asset_dict.abbreviation
        self._price.live = self.live
        self._price.episode = self.episode
        return self._price

    @property
    def technicals(self) -> TAPriceData:
        self._technical.processor = self.processor
        self._technical.name = self.asset_dict.name
        self._technical.subcategories = self.asset_dict.subcategories
        self._technical.abbreviation = self.asset_dict.abbreviation
        self._technical.live = self.live
        self._technical.episode = self.episode
        return self._technical

    @property
    def machine(self):
        """Stacked State Space

        Return the stacked state space. The stacked state space is n number of steps
        """
        return self._space

    @property
    def incremental(self) -> IncrementalEngine:
        """Stacked State Space

        Return the stacked state space. The stacked state space is n number of steps
        """

        self._incremental_engine.processor = self.processor
        self._incremental_engine.name = self.asset_dict.name
        self._incremental_engine.category = self.asset_dict.category
        self._incremental_engine.subcategories = self.asset_dict.subcategories
        self._incremental_engine.abbreviation = self.asset_dict.abbreviation
        self._incremental_engine.submetatype = self.asset_dict.submetatype
        self._incremental_engine.live = self.live
        self._incremental_engine.episode = self.episode
        return self._incremental_engine

    @property
    def current_time(self) -> float:
        return super().time.head

    @property
    def update_dict(self) -> Dict[str, Any]:
        return {
            "episode": self.episode,
            "live": self.live,
            "time": self.current_time, # "timestamp": self.current_time
        }


__all__ = ['StateWithProperties']
