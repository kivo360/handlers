from dataclasses import dataclass
from enum import Enum


class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3


@dataclass
class IndexData:
    '''Class for keeping track of an item in inventory.'''
    x_index: int = 0
    y_pred: int = 0
    y_real: int = 0


class BaseIndexer:
    """Base Level Index Information

        Use to determine what location you should pull x, y, and y_hat variables.
        
        It's 100% activity based. Each call returns a base model.
        
        ```py
            indexing = Indexing()
            loss_index = indexing.get_loss()
            pred_index = indexing.get_prediction()
            training_index = indexing.get_training()
        ```
    """

    def __init__(self, look: int = 1) -> None:
        # The current index
        self.__index: int = 0
        # How many steps we're trying to predict for
        self.look = look
        # The response we're giving to the user
        self.index_data = IndexData()

    def loss(self) -> None:
        self.index_data.y_pred = self.__index - self.look
        self.index_data.y_real = self.__index
        self.index_data.x_index = self.__index

    def train(self) -> None:
        self.index_data.y_pred = self.__index
        self.index_data.y_real = self.__index
        self.index_data.x_index = self.__index - self.look

    def pred(self) -> None:
        """
            Set Index To Make And Store Predictions
        """
        self.index_data.y_pred = self.__index + self.look
        self.index_data.y_real = self.__index
        self.index_data.x_index = self.__index

    @property
    def x_index(self) -> int:
        return self.index_data.x_index

    @property
    def y_index(self) -> int:
        return self.index_data.y_real

    @property
    def y_target(self) -> int:
        return self.index_data.y_pred


@dataclass
class TrackingIndexes:
    """Tracking Indexes

    Return the y variables to track.
    """
    y_index_head: int = 0
    y_index_tail: int = 0

    y_target_head: int = 0
    y_target_tail: int = 0


@dataclass
class InputIndexes:
    """Input Indexes

    Return the X variables you'd use to track.
    """
    x_index: int = 0
    x_tail: int = 0


class Indexer(BaseIndexer):

    def __init__(self, ahead: int = 1, window_size: int = 1) -> None:
        super().__init__(ahead)
        self.__window_size: int = window_size
        self.__tracking: TrackingIndexes = TrackingIndexes()
        self.__input: InputIndexes = InputIndexes()

    @property
    def action(self) -> InputIndexes:
        self.__input.x_index = self.x_index
        self.__input.x_tail = self.x_index - self.__window_size
        return self.__input

    @property
    def state(self) -> InputIndexes:
        self.__input.x_index = self.x_index
        self.__input.x_tail = self.x_index - self.__window_size
        return self.__input

    @property
    def reward(self) -> TrackingIndexes:
        self.__tracking.y_index_head = self.y_index
        self.__tracking.y_index_tail = self.y_index - self.__window_size
        self.__tracking.y_target_head = self.y_target
        self.__tracking.y_target_tail = self.y_target - self.__window_size
        return self.__tracking

    def price(self) -> TrackingIndexes:
        self.__tracking.y_index_head = self.y_index
        self.__tracking.y_index_tail = self.y_index - self.__window_size
        self.__tracking.y_target_head = self.y_target
        self.__tracking.y_target_tail = self.y_target - self.__window_size
        return self.__tracking

    def risk(self) -> TrackingIndexes:
        self.__tracking.y_index_head = self.y_index
        self.__tracking.y_index_tail = self.y_index - self.__window_size
        self.__tracking.y_target_head = self.y_target
        self.__tracking.y_target_tail = self.y_target - self.__window_size
        return self.__tracking