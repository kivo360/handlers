import copy
import random

import uuid

from typing import Any, AnyStr, Dict, List, Optional, Union

import maya
import numpy as np
import pandas as pd
from jamboree import DBHandler, Jamboree
from jamboree.handlers.default import MultiDataManagement, TimeHandler
from jamboree.handlers.default.data import DataHandler
from jamboree.utils import omit
from loguru import logger

from see137.data import PriceGenerator
from see137.handlers.allocation_ray import AllocationHandlerRay
from see137.handlers.parallel import allocation
from see137.parameters.exchange import UpdateableExchangeState
from see137.utils.slippage import RandomUniformSlippageModel
from see137.utils.trades.trade import DynamicTrade, Trade, TradeType

# from pypfopt import black_litterman, risk_models
# # from crayons import blue, cyan, green, magenta, red, white, yellow
# from toolz import keyfilter, pluck

#
# I need Arunavo to finish order management.
#
# logger.disable(__name__)
"""
Note: Will remove soon. Going through a concept.
"""


class PortfolioHandler(DBHandler):
    """Abstract handler that we use to keep track of portfolio information.
    """

    def __init__(
        self,
        base_instrument="USD",
        start_balance=10000,
        limit=100,
        lazy_load=False
    ):
        super().__init__()
        self.entity = "portfolio"
        self.required = {
            "episode": str, "user_id": str, "exchange": str, "live": bool
        }

        # TODO: Lazy load should be replaced with the parameters `pre_load` and `just_in_time`
        self._base_instrument = base_instrument
        self.start_balance = start_balance
        # TODO: Cut down on the object initiation.
        self._time: TimeHandler = TimeHandler()
        self._data: MultiDataManagement = MultiDataManagement()
        self._allocator: Optional[allocation.AllocationHandler
                                  ] = allocation.AllocationHandler()

        self._balance = 0
        self.limit = limit
        self._subcategories = {}

        # Store a list of the portfolio & performance states

        self._slippage_model = RandomUniformSlippageModel()
        # TODO: Need to replace
        self._instrument_precision = 8
        self._commission_percent = 0.3
        self._base_precision = 2

        self._min_trade_amount = 0.000001
        self._max_trade_amount = 1000000
        self._min_trade_price = 0.00000001
        self._max_trade_price = 100000000
        self._state = UpdateableExchangeState()

        # Placeholder variables
        self.placeholders = {}
        self.latest_prices = {}
        # Determines if the object has loaded the important variables. Use to prevent loading again
        self.has_updated = False
        self.is_inner_allocation = False
        self.is_sim = False
        self.allocation_updated = False
        # TODO: Figured out using dataclass to manage the core variables.

    # -------------------------------------------------------------------
    # --------------------- Properties & Setters ------------------------
    # -------------------------------------------------------------------

    @property
    def exchange_state(self) -> UpdateableExchangeState:
        return self._state

    @property
    def is_live(self) -> bool:
        lively = self['live']
        episode = self['episode']
        if episode == "live" and lively == True:
            return True
        return False

    @property
    def current_time(self):
        """ 
            # Current Time
            ---
            Get the current time for the portfolio.

            Gets the time from the price/dataset. If the datasource is live it should get the current price and use that. 
        """
        return self.time.head

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, limit):
        self._limit = limit

    @property
    def user_id(self) -> str:
        return self['user_id']

    @user_id.setter
    def user_id(self, _user_id: str):
        self['user_id'] = _user_id

    @property
    def exchange(self) -> str:
        return self['exchange']

    @exchange.setter
    def exchange(self, _exchange: str):
        self['exchange'] = _exchange

    @property
    def episode(self) -> str:
        return self['episode']

    @episode.setter
    def episode(self, _episode: str):
        self['episode'] = _episode

    @property
    def holdings(self):
        """ Get the holdings for the user """
        self.update_if_hasnt()

        return self.exchange_state.holdings.holdings

    @property
    def performance(self):
        """ Get the performance of the user/exchange"""
        self.update_if_hasnt()
        return self.exchange_state.performance

    def update_if_hasnt(self, forced=False):
        if not self.has_updated or forced is True:
            self.load_placeholders()

    @property
    def portfolio(self):
        """ Get the latest portfolio of the user/exchange"""
        self.update_if_hasnt()
        return self.exchange_state.portfolio.portfolio

    @property
    def balance(self) -> float:
        self.update_if_hasnt()
        return self.exchange_state.balance

    @property
    def trades(self) -> List[Dict]:
        """ Get all of the trades for this user"""
        trades = self.load_trades()
        if isinstance(trades, dict):
            trades = [trades]
        filtration_list = [
            "episode",
            "exchange",
            "user_id",
            "type",
            "detail",
            "timestamp",
            "live",
            'event_id'
        ]
        trades_filtered = map(lambda x: omit(filtration_list, x), trades)
        return list(trades_filtered)

    @property
    def net_worth(self) -> float:

        net_worth = self.balance
        portfolio = self.portfolio
        # logger.info(net_worth)
        # logger.info(portfolio)
        if not portfolio:
            return net_worth

        for symbol, amount in portfolio.items():
            if symbol == self._base_instrument:
                continue

            current_price = self.current_price(symbol=symbol)
            net_worth += current_price * amount

        return float(net_worth)

    @property
    def pricing(self):
        """ 
            NOTE: Update the pricing access mechanism during mega refactor.
                - We determine the user's portfolio using this dataset.
                - Since it has a name heavily tailored to the
                - We position what we want the user to have using this pricing indicator 
        """
        if self._data is None:
            raise NotImplementedError("Pricing handler not found")
        user_id = self['user_id']
        exchange = self['exchange']
        episode = self['episode']
        live = self['live']

        self._data['name'] = f"{user_id}-{exchange}-{episode}-{live}"
        self._data['category'] = "portfolio_info"
        self._data['subcategories'] = {
            "real": False
        }
        self._data['metatype'] = "port"
        self._data['submetatype'] = "lio"
        self._data['abbreviation'] = "UPORT"

        self._data.episode = episode
        self._data.live = live
        self._data.processor = self.processor
        # self._data.reset()
        return self._data

    def is_user_asset(self, asset: dict) -> bool:
        """ Determines if the given asset is indeed in the user's portfolio """
        asset_keys = list(asset.keys())
        # logger.error(asset_keys)
        if len(asset_keys) == 0:
            return False

        # TODO: Fix the source of this
        if 'episode' in asset_keys:
            episode = asset.pop('episode', None)
            asset['subcategories']['episode'] = episode
        # logger.error(asset_keys)
        current_user_assets = self.users_assets
        # logger.info(current_user_assets[0])
        # logger.success(asset)
        if len(current_user_assets) == 0:
            return False
        hashed_comparision_asset = self.main_helper.generate_hash(asset)
        # logger.info(hashed_comparision_asset)
        comparison_hashed_assets = [
            self.main_helper.generate_hash(x) for x in current_user_assets
        ]
        return hashed_comparision_asset in comparison_hashed_assets

    @property
    def users_assets(self) -> list:
        return self.pricing.sources

    @property
    def allocation(self) -> dict:
        self.update_if_hasnt()
        return self.exchange_state.allocation.allocation

    @property
    def allocator(self):
        """ Allocation stuff for"""
        if self._allocator is None:
            raise AttributeError("Allocator handler not found")

        user_id = self['user_id']
        exchange = self['exchange']
        episode = self['episode']
        live = self['live']

        self._allocator["episode"] = episode
        self._allocator["user_id"] = user_id
        self._allocator["exchange"] = exchange
        self._allocator["live"] = live
        self._allocator.processor = self.processor
        self._allocator.time = self.time
        return self._allocator

    @property
    def profit_loss_percent(self) -> float:
        """
            TODO: Turn this into something that uses a window instead of an absolute time. The windowing function can be determined using the state class. 
            Calculate the percentage change in net worth since the last reset.
            Returns:
                The percentage change in net worth since the last reset.
        """
        return float(self.net_worth / self.start_balance) * 100

    # Use to get counts inside of the database

    @property
    def time(self) -> 'TimeHandler':
        self._time.processor = self.processor
        self._time["live"] = self["live"]
        self._time["episode"] = self["episode"]
        # self._time.reset()
        return self._time

    def batch_count(self) -> int:
        alt = {
            "detail": "batch"
        }
        count = self.count(alt)
        return count

    def _get_mock_backpressure(
        self,
        start: float,
        stop: float,
        backpressure_prob: float = 0.1,
        detail_name: str = "amount"
    ):
        mock_name = f"{detail_name}_mock"
        alt = {
            "detail": mock_name
        }
        latest_value = self.get_single(alt=alt)
        is_backpressure = (random.uniform(0, 1) < backpressure_prob)
        if not bool(latest_value) or not is_backpressure:
            value = random.uniform(0, 30)
            val_dict = {
                "value": value
            }
            self.set_single(val_dict, alt=alt)
            return value
        return latest_value['value']

    def current_amount(
        self,
        symbol: str,
        is_mocked=False,
        backpressure_prob: float = 0.1
    ) -> float:
        """Get the amount (count) of a given asset.

        Args:
            symbol (str): The name of the asset we're looking for.
            is_mocked (bool, optional): Used to set a mock. Returns a random variable if it's set to true. Defaults to False.

        Returns:
            float: The amount of the assets in the portfolio.
        """
        if is_mocked is True:
            return self._get_mock_backpressure(
                0,
                30,
                backpressure_prob=backpressure_prob,
                detail_name="amount"
            )
        # self.portfolio
        # self.load_placeholders()
        # portfolio = self.portfolio
        # how many of this given asset do we have
        amount_value = self.exchange_state.current_amount(symbol)
        return float(amount_value)

    def current_allocation(
        self,
        symbol="BTC",
        is_mocked=False,
        backpressure_prob: float = 0.1
    ) -> float:
        """Get the current allocation of a given asset.

        Args:
            symbol (str, optional): The symbol we're looking to get allocations for. Defaults to "BTC".

        Returns:
            float: The allocation of the symbol. Defaults to 0.0 if we can't find it.
        """
        if is_mocked is True:
            return self._get_mock_backpressure(
                0,
                0.999,
                backpressure_prob=backpressure_prob,
                detail_name="allocation"
            )
        allocation_value = self.exchange_state.current_allocation(symbol)
        return float(allocation_value)

    def current_pct_worth(
        self,
        symbol: str = "BTC",
        is_mocked: bool = True,
        backpressure_prob: float = 0.1
    ) -> float:
        """current_pct_worth Percentage Of Networth

        Get the percentage of the networth for the given symbol.

        Args:
            symbol (str, optional): The symbol we're checking. Defaults to "BTC".
            is_mocked (bool, optional): Determines if we're mocking the results. Defaults to True.

        Returns:
            float: Percentage of networth the asset takes.
        """
        net_worth = self.net_worth
        current_value = self.current_total(
            symbol=symbol,
            is_mocked=is_mocked,
            backpressure_prob=backpressure_prob
        )
        # logger.success(net_worth)
        # logger.warning(current_value)
        return current_value / net_worth

    def current_pct_limit(
        self,
        symbol="BTC",
        is_mocked: bool = False,
        backpressure_prob: float = 0.1
    ) -> float:
        """current_pct_limit Get the percentage of limit.

        Given the portfolio allocation cap that we've set. The allocation cap is the maximum amount we're allowed to trade on within a given time period.

        Args:
            symbol (str, optional): Symbol to get info for. Defaults to "BTC".
            is_mocked (bool, optional): Determines if we're mocking. Defaults to False.

        Returns:
            float: The percentage of the maximum allocation cap.
        """
        pct_worth: float = self.current_pct_worth(
            symbol=symbol,
            is_mocked=is_mocked,
            backpressure_prob=backpressure_prob
        )
        allocation_pct: float = self.current_allocation(
            symbol=symbol,
            is_mocked=is_mocked,
            backpressure_prob=backpressure_prob
        )
        if allocation_pct == 0:
            return 0.0
        return pct_worth / allocation_pct

    def current_price(
        self,
        symbol: str = "BTC",
        is_mocked: bool = False,
        backpressure_prob: float = 0.1
    ) -> float:
        """current_price Get the price of `symbol`.

        Returns the current price of the given asset.

        if `is_mocked` is `True` we return a random number between 0 and 1000. 

        Args:
            symbol (str, optional): The symbol we're getting the price for. Defaults to "BTC".
            is_mocked (bool, optional): Determines if we're mocking the price. Defaults to False.


        Returns:
            float: The current price.
        """
        if is_mocked is True:
            return self._get_mock_backpressure(
                0,
                1000,
                backpressure_prob=backpressure_prob,
                detail_name="price"
            )

        return self.exchange_state.current_price(symbol)

    def current_total(
        self,
        symbol: str = "BTC",
        is_mocked: bool = False,
        backpressure_prob: float = 0.1
    ) -> float:
        """# Total Value

        Gets the total value of the given symbol.

        ```
            total = price * amount
        ```

        Args:
            symbol (str, optional): The symbol of the asset we're looking to get price information for. Defaults to "BTC".
            is_mocked (bool, optional): Determines if we're mocking the price. Defaults to False.

        Returns:
            float: The total value of the asset given in the symbol.
        """
        price = self.current_price(
            symbol=symbol,
            is_mocked=is_mocked,
            backpressure_prob=backpressure_prob
        )
        amount = self.current_amount(
            symbol=symbol,
            is_mocked=is_mocked,
            backpressure_prob=backpressure_prob
        )
        return price * amount

    # ----------------------------------------
    # -------------- Querying ----------------
    # ----------------------------------------

    def load_trades(self):
        alt = {
            "detail": "trade"
        }
        if self.is_live == True:
            trades = self.many(self.limit, alt=alt, ar="absolute")
        else:
            trades = self.many(self.limit, alt=alt, ar="relative")
        if isinstance(trades, dict):
            return [trades]
        return trades

    def latest_state(self) -> Dict[AnyStr, Any]:
        """
            batch_load_exchange Batch Load All Data

            Batch load all data into place to reduce the number of calls in total. 

            Should load the placeholders object in aggregate instead of separately (latency reduction and higher speeds).

            Will run along-side the existing load and save placeholders to

        Returns:
            bool: True if the data was successfully saved.
        """
        alt = {
            "detail": "batch"
        }
        if self.is_live == True:
            last_state = self.last(alt=alt, ar="absolute")
            return last_state
        last_state = self.last(alt=alt, ar="relative")
        return last_state

    def load_placeholders(self):
        if self.has_updated == False:
            latest = self.latest_state()
            self.exchange_state.reset(latest)

            self.has_updated = True

    # ----------------------------------------
    # ---------------- Saving ----------------
    # ----------------------------------------

    #

    def save_trade(self, data):
        alt = {
            "detail": "trade"
        }

        _data = copy.copy(self._query)
        _data.update(data)
        _data.update(alt)

        _data['type'] = self.entity
        _data['time'] = self.current_time
        _data['timestamp'] = maya.now()._epoch
        for key, value in _data.items():
            if isinstance(value, np.float64):
                _data[key] = float(value)
        self.save(_data, alt=alt)

    def save_batch(self, data):
        alt = {
            "detail": "batch"
        }

        _data = copy.copy(self._query)
        _data.update(data)
        _data.update(alt)

        self.save(_data, alt=alt)

    # ----------------------------------------
    # ------------ Operations ----------------
    # ----------------------------------------

    def _update_account(self, _trade: Trade):
        """ 
            Updates the portfolio exchange account. 

            TODO: Add net worth and balance field to the new dataclass
        """

        if self._is_valid_trade(_trade):
            self._make_trade(_trade)

        # current_balance = float(self.balance)
        portfolio = self.portfolio
        portfolio[self._base_instrument] = self.balance

        self.exchange_state.net_worth = self.net_worth
        self.exchange_state.balance = self.balance
        self._update_holdings()

    def _update_account_no_order(self):
        """ Updates the portfolio exchange account. """
        # TODO: Add timestamp to trade
        current_balance = float(self.balance)
        base_instrument = self._base_instrument
        self.exchange_state.portfolio.set_asset(
            base_instrument, current_balance
        )
        self._update_holdings()

    def _update_holdings(self):
        """ Duplication of net_worth code"""
        holdings = {}
        portfolio = self.portfolio
        if not portfolio:
            return

        portfolio_items = portfolio.items()
        for symbol, amount in portfolio_items:
            if symbol == self._base_instrument:
                holdings[symbol] = amount
                continue

            current_price = self.current_price(symbol=symbol)
            total = current_price * amount
            holdings[symbol] = total

        self.exchange_state.holdings.reinitialize(holdings)

    def _make_trade(self, trade: Trade):
        """ Trades on the account then updates it"""

        if not trade.is_hold:
            """ TODO: We'll need to save this in two places to make it work IRL"""
            self.save_trade({
                'symbol': trade.symbol,
                'trade_type': trade.trade_type.value,
                'amount': trade.amount,
                'price': trade.price
            })

        self.exchange_state.make_trade(trade)

    def _is_valid_trade(self, _trade: Trade) -> bool:
        trade_amt = _trade.amount
        total = _trade.amount * _trade.price
        symbol = _trade.symbol

        instrument_amount = self.current_amount(symbol=symbol)

        if _trade.is_buy and self.balance < total:
            logger.error("Not enough to buy")
            return False
        elif _trade.is_sell and instrument_amount < trade_amt:
            logger.error("Not enough to sell")
            return False

        is_both = _trade.amount >= self._min_trade_amount and _trade.amount <= self._max_trade_amount

        return is_both

    def execute_trade(self, trade: Trade) -> Trade:
        self.load_placeholders()

        current_price = self.current_price(symbol=trade.symbol)
        commission = self._commission_percent / 100
        filled_trade = trade.copy()

        filled_price = filled_trade.price
        if filled_price == 0:
            filled_price = 0.00000000001

        if filled_trade.is_hold or not self._is_valid_trade(filled_trade):
            filled_trade.amount = 0

        if filled_trade.is_buy:
            price_adjustment = (1 + commission)
            filled_trade.price = round(
                current_price * price_adjustment, self._base_precision
            )
            # filled_trade.amount = 0.00000000001
            filled_trade.amount = round(
                (filled_trade.price * filled_trade.amount) / filled_price,
                self._instrument_precision
            )
            # with logger.catch(exception=ZeroDivisionError, reraise=True):
        elif filled_trade.is_sell:
            price_adjustment = (1 - commission)
            filled_trade.price = round(
                current_price * price_adjustment, self._base_precision
            )
            filled_trade.amount = round(
                filled_trade.amount, self._instrument_precision
            )

        if not filled_trade.is_hold:
            filled_trade = self._slippage_model.fill_order(
                filled_trade, current_price
            )

        self._update_account(filled_trade)
        return filled_trade

    # -------------------------------------------------------
    # ------------------ Reset Conditions -------------------
    # -------------------------------------------------------

    def _reset_batch_holdings(self):
        """
            # Reset Holdings

            Holdings is the absolute networth of every asset inside of the portfolio. Based entirely on (price * amount).

            # TODO: Set the starting balance in the PortfolioState instead of using the save holdings command.
        """

        count = self.batch_count()
        if count == 0:
            _base: str = self._base_instrument
            _balance: float = float(self.start_balance)
            _hold: dict = {
                _base: _balance
            }
            _allo: dict = {}
            _port: dict = {
                _base: _balance
            }

            self.exchange_state.net_worth = _balance
            self.exchange_state.balance = _balance
            self.exchange_state.allocation.reinitialize(_allo)
            self.exchange_state.holdings.reinitialize(_hold)
            self.exchange_state.portfolio.reinitialize(_port)
            self.exchange_state.time = self.current_time

            self.save_batch(self.exchange_state.extract())
            loader = self.latest_state()
            logger.info(loader)
            self.exchange_state.reset(loader)
        else:
            self.update_if_hasnt()

    def _reset_price(self):
        """ Resets the price. If it's a backtest or simulation, we'll pull information into place so we can backtest. """
        self.pricing.reset()

    def _reset_allocation(self):
        self.allocator.reset()

    def _reset_time(self):
        self.time.reset()

    def add_asset(
        self,
        name,
        subcategories={},
        extras={
            "submetatype": "", 'abbreviation': ""
        }
    ):
        """This can only be under the category market"""
        category = "markets"
        combined_dict = {
            "name": name, "category": category, "subcategories": subcategories
        }
        combined_dict.update(extras)
        if self.is_sim:
            combined_dict["subcategories"]['episode'] = self['episode']
        self.pricing.add_multiple_data_sources([combined_dict])

    def remove_asset(self, name, subcategories={}):
        """
            TODO: Add remove asset command to the class during the hardening.
            Remove assets from the user. 
            It doesn't add if it doesn't exist.
        """
        pass

    def price_preprocessing(self, data: dict):
        data_keys = list(data.keys())
        first_item = [x.split(":")[0] for x in data_keys]
        portfolio = pd.DataFrame()
        latest_price_dict = {}
        for index, first in enumerate(first_item):
            dk = data_keys[index]
            d1 = data[dk]
            portfolio[str(first)] = d1['close']
            latest_price_dict[first] = d1['close'].iloc[-1]
        return portfolio, latest_price_dict

    def price_preprocessing_current(
        self, data: Dict[str, Dict[Any, Any]]
    ) -> Dict[str, Any]:
        """ Get all of the information and return the latest price dict"""
        latest_price_dict = {}
        for key, value in data.items():
            first = key.split(":")[0]
            close = value.get("close", 0.0)
            latest_price_dict[first] = close
        return latest_price_dict

    def clear_cache(self):
        self.placeholders = {}
        self.has_updated = False

    def clear_update(self):
        self.clear_cache()
        self.update_if_hasnt()

    def sim_only_data(self, datasets):
        if self.is_sim == False:
            return

        if len(datasets) == 0:
            return
        subcategory = datasets[0]['subcategories']
        subcategory['episode'] = self['episode']
        new_list = []
        for dataset in datasets:
            dataset['subcategories'] = subcategory
            new_list.append(dataset)
        self._data.data_handler_list = new_list

    def step_price(self, forced: bool = False) -> None:
        self.pricing.sync()
        if self.allocator.should_rebalance and forced:
            prices_information = self.pricing.step()
            portfolio_batch, price_dict = self.price_preprocessing(
                prices_information,
            )
            self.exchange_state.prices.reinitialize(price_dict)
            with logger.catch():
                self.allocator.step(portfolio_batch)
            self.has_updated = False
            self.update_if_hasnt(forced=True)
            return

        prices_information = self.pricing.step(call_type="current")
        price_dict = self.price_preprocessing_current(prices_information)
        self.exchange_state.prices.reinitialize(price_dict)
        self.allocator._finish_step()

    def step_trade(self, trade: Union[Trade, DynamicTrade] = None) -> None:
        if trade is not None:
            if isinstance(trade, DynamicTrade):
                local_trade: DynamicTrade = trade
                net_worth = self.net_worth
                price: float = self.current_price(symbol=local_trade.symbol)
                amount: float = self.current_amount(symbol=local_trade.symbol)
                cap_pct: float = self.current_allocation(
                    symbol=local_trade.symbol
                )

                dynamic_trade: DynamicTrade = copy.copy(local_trade)
                dynamic_trade.cap_pct = cap_pct

                dynamic_trade.amount = amount
                dynamic_trade.price = price
                dynamic_trade.net_worth = net_worth

                trade = dynamic_trade.calculate_trade()

                if isinstance(trade.amount, np.float64):
                    trade.amount = float(trade.amount)

            if isinstance(trade, Trade):

                self.execute_trade(trade)

            else:
                self._update_account_no_order()
        else:
            self._update_account_no_order()

    def save_and_clear(self):
        allocations = self.allocator.latest_allocation()

        if 'allocation' in allocations:
            __all = allocations['allocation']
            self.exchange_state.allocation.reinitialize(__all)

        current_state = self.exchange_state.extract()
        self.save_batch(current_state)
        self.clear_cache()

    def step_allocation(self):
        logger.critical("Rebalancing portfolio")
        self.update_if_hasnt()
        self.step_price(forced=True)
        self.save_and_clear()
        return {
            "msg": "We've allocated!!!!!!!"
        }

    def step(self, trade: Optional[Union[Trade, DynamicTrade]] = None) -> Dict:
        final_dict = {
            'obs': {}, 'done': False
        }
        """ It's a step sandwich"""
        self.update_if_hasnt()
        self.step_price()
        self.step_trade(trade)

        # This doesn't load if we've stepped through and reallocated the class internally
        # Should replace this with batch_load_exchange
        # How you initialize all of the variables should be independent.

        final_dict['done'] = (not self.pricing.is_next)
        # we'd put all of the portfolio information here by asset.
        final_dict['obs']["portfolio"] = {}
        # get percentages
        self.save_and_clear()
        return final_dict

    def render(self):
        """ Get the information necessary to give a report for the user. """
        self.load_placeholders()

        performance_frame: pd.DataFrame = pd.DataFrame(self.performance)
        holdings_frame: pd.DataFrame = pd.DataFrame(self.holdings)
        return (performance_frame, holdings_frame)
        # portfolio_frame  :pd.DataFrame = pd.DataFrame(self.portfolio_history)

    def reset(self):
        """ Determines if we're re-initiating """
        # logger.warning("Resetting Portfolio")
        self.check()
        self._reset_price()
        self._reset_time()
        self._reset_allocation()
        """ Portfolio specific resets """
        self._reset_batch_holdings()


class AsyncPortfolioHandler:

    def __init__(self, host: str = "localhost", port: int = 6379) -> None:
        self._portfolio = PortfolioHandler()
        self._portfolio.processor = Jamboree(REDIS_HOST=host, REDIS_PORT=port)

    def set_parameters(self, **kwargs):
        for k, v in kwargs.items():
            self._portfolio[k] = v

    def step(self, trade: Optional[Trade] = None, **kwargs):
        allocation_only = kwargs.pop("allocation_only", False)
        self.set_parameters(**kwargs)
        self.reset()
        if allocation_only:
            return self._portfolio.step_allocation()
        return self._portfolio.step(trade)

    def render(self):
        return self._portfolio.render()

    def reset(self):
        self._portfolio.reset()


def get_base_datahandler():
    datahandler = DataHandler()
    datahandler["category"] = "markets"
    datahandler["subcategories"] = {
        "market": "stock", "country": "US", "sector": "faaaaake"
    }
    return datahandler


def price_gen_func(
    episodes: List[str],
    asset_list=["BTC", "ATL", "TRX", "ETH", "BCH", "XRP"],
    number_of_episodes=10
):
    if episodes is None:
        episodes = [uuid.uuid4().hex for x in range(number_of_episodes)]
    price_gen = PriceGenerator()
    price_gen.episodes = episodes
    price_gen.assets = asset_list
    price_gen.starting_min = 20
    price_gen.starting_max = 5000
    price_gen.is_varied = True
    price_gen.length = 3000
    price_gen.generate_all_episodes()
    return price_gen


@logger.catch(message="Because we never know...", reraise=True)
def main():
    """ Should run through everything returning zero right now"""
    asset_list = ["BTC", "ATL", "TRX", "ETH", "BCH", "XRP"]

    main_episode = uuid.uuid4().hex
    user_id = uuid.uuid4().hex
    exchange_name = "fake_exchange"
    episodes = [main_episode]
    prices = price_gen_func(
        episodes=episodes, asset_list=asset_list
    ).episode_set

    processor = Jamboree()

    portfolio_hand = PortfolioHandler()
    portfolio_hand.processor = processor
    portfolio_hand['exchange'] = exchange_name
    portfolio_hand['user_id'] = user_id
    portfolio_hand['episode'] = main_episode
    portfolio_hand['live'] = False
    portfolio_hand.reset()
    portfolio_hand.time.head = maya.now().add(weeks=25)._epoch
    portfolio_hand.time.change_stepsize(microseconds=0, days=1, hours=0)
    portfolio_hand.time.change_lookback(microseconds=0, weeks=25, hours=0)

    dh = get_base_datahandler()
    dh.processor = processor
    # dh.name = ""
    dh.reset()
    subcat = dh['subcategories']
    portfolio_hand.is_sim = True
    for episode in prices.keys():
        logger.warning(f"Episode: {episode}")
        for asset in asset_list:
            _subcat = copy.copy(subcat)
            _subcat['episode'] = episode
            dh.name = asset
            dh['subcategories'] = _subcat
            data = prices[episode][asset]
            dh.reset()
            dh.store_time_df(data, is_bar=True)

    portfolio_hand.add_asset(asset_list[0], subcat)
    portfolio_hand.add_asset(asset_list[1], subcat)
    portfolio_hand.add_asset(asset_list[2], subcat)
    portfolio_hand.is_inner_allocation = True
    portfolio_hand.step_price()
    for _ in range(100):
        if random.uniform(0, 1) < 0.1:
            symbol = random.choice([
                asset_list[0], asset_list[1], asset_list[2]
            ])
            trade_type = random.choice([
                TradeType.LIMIT_BUY, TradeType.LIMIT_SELL
            ])
            dynamic_trade = DynamicTrade(symbol, trade_type)
            dynamic_trade.percentage = random.normalvariate(0.3, 0.06)
            portfolio_hand.step(dynamic_trade)
        else:
            portfolio_hand.step()
        portfolio_hand.time.step()
    portfolio_hand.render()


if __name__ == "__main__":
    main()
