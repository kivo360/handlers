import pytest
import asyncio
from xprocess import ProcessStarter
import sys
from see137.utils.asyncs import subcommands

from loguru import logger

logger.configure(
    handlers=[
        dict(sink=sys.stdout, colorize=True, enqueue=True),
    ]
)