from multiprocessing import cpu_count
import random
import pandas as pd
import ray
from jamboree import Jamboree
from jamboree.handlers import TimeHandler
from ray.util import ActorPool
from loguru import logger
from see137.handlers.allocation import AllocationHandler

CPU_NUM = cpu_count()


@ray.remote
class AllocationActor(object):

    def __init__(self):
        self.processor = Jamboree()
        self.allocator = AllocationHandler()
        self.timer = TimeHandler()
        self.allocator.processor = self.processor
        self.timer.processor = self.processor

    def set_time(self, episode: str, live: bool):
        self.timer['episode'] = episode
        self.timer['live'] = live

    def allocate_remote_and_finish(
        self,
        rebalance_frame: pd.DataFrame,
        user_id: str,
        exchange: str,
        episode: str,
        live: bool = False
    ):
        self.set_time(episode, live)
        self.allocator["episode"] = episode
        self.allocator["user_id"] = user_id
        self.allocator["exchange"] = exchange
        self.allocator["live"] = live
        self.allocator.time = self.timer
        self.allocator.reset()
        self.allocator._allocation_and_finish(rebalance_frame)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton,
                                        cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]


class ActorManager(metaclass=Singleton):
    ACTORS = None

    def __init__(self):
        pass

    @logger.catch(reraise=True)
    def init_actors(self):
        if self.ACTORS is None:
            self.ACTORS = [
                AllocationActor.remote() for x in range(CPU_NUM // 2)
            ]

    def get_actors(self):
        self.init_actors()
        return random.choice(self.ACTORS)


actingboi = ActorManager()
