from maya.core import MayaDT
from see137.handlers.states import indexing, machine
from see137.importers import *
from jamboree.utils import omit


def omit_single(x):
    omission_list = [
        'submetatype',
        'metatype',
        'category',
        'subcategories',
        'type',
        'mtype',
        'detail',
        'live',
        'episode',
        'exchange',
        'user_id',
        'abbreviation',  # 'name',
        'timestamp',
    ]
    return omit(omission_list, x)


def omit_set(x_set):
    results = list(map(omit_single, x_set))
    return results


def omitted_df(x_set):
    oset = omit_set(x_set)
    df = pd.DataFrame(oset)
    with logger.catch():
        df['time'] = pd.to_datetime(df['time'], unit='s')
    return df


def log_head_tail(
    head: Union[int, float], tail: Union[int, float], name: str = "state"
):
    logger.opt(colors=True).info(
        "<magenta> {name} Indexes </magenta> \t| Head Index: {head},  Tail Index: {tail}",
        head=head,
        tail=tail,
        name=name.capitalize()
    )


def set_pred_name(x: dict) -> dict:
    x['time'] = x['ptime']
    return x


class __PrivateEnv(machine.StateSpaceMachine):

    def __init__(self, prediction_lag: int = 3, window_size: int = 1):
        """
            Use to determine exactly what you should reset at the beginning.
        """
        super().__init__(prediction_lag)
        self.indexer = indexing.Indexer(
            ahead=prediction_lag, window_size=window_size
        )

    def state_frame(self) -> Optional[pd.DataFrame]:

        # Get and adjust variables
        state_count = self.state_count()

        if state_count == 0:
            return None

        state = self.indexer.state
        head = state.x_index
        tail = state.x_tail

        # Access Core Variables and DataFrame
        state_set = super().load_state_set(num=tail, start=head)
        state_set_len: int = len(state_set)

        if state_set_len == 0:
            return None

        state_set_df = omitted_df(state_set)
        # Log some information
        return state_set_df

    def action_frame(self) -> Optional[pd.DataFrame]:
        # Get and adjust variables
        if self.action_count() == 0:
            return None

        state = self.indexer.action
        head = state.x_index
        tail = state.x_tail

        # Access Core Variables and DataFrame
        state_set = super().load_action_set(num=tail, start=head)
        state_set_len: int = len(state_set)

        if state_set_len == 0:
            return None
        state_set_df = omitted_df(state_set)
        # Log some information
        return state_set_df

    def reward_frame(self) -> Optional[pd.DataFrame]:

        if super().reward_count() == 0:
            return None

        _reward = self.indexer.reward
        tail = _reward.y_index_tail
        head = _reward.y_index_head
        # distance = abs(head - tail)

        reward_set = super().load_reward_set(num=tail, start=head)
        reward_set_len: int = len(reward_set)

        if reward_set_len == 0:
            return None

        reward_set_df = omitted_df(reward_set)

        return reward_set_df

    def reward_pred_frame(self) -> Optional[pd.DataFrame]:

        if super().reward_count() == 0:
            return None

        _reward = self.indexer.reward
        tail = _reward.y_target_tail
        head = _reward.y_target_head

        reward_pred_set = super().load_estimation_set(num=tail, start=head)
        reward_pred_set_len: int = len(reward_pred_set)

        if reward_pred_set_len == 0:
            return None

        # We set the prediction time to the real time.
        reward_pred_set = list(map(set_pred_name, reward_pred_set))
        reward_set_df = omitted_df(reward_pred_set)
        reward_set_df = reward_set_df.drop(columns=['ptime'])
        return reward_set_df

    def step_train(self) -> np.ndarray:
        """Step Train

        Get the training data frame for the Gaussian process. Should preprocess prior to returning.

        Returns
        -------
        np.ndarray
            The array to train the agent.
        """
        # Get multiple states
        self.indexer.train()
        state_frame = self.state_frame()
        reward_frame = self.reward_frame()
        action_frame = self.action_frame()
        if state_frame is None or reward_frame is None or action_frame is None:
            # Create rules to skip the rest of the logic
            return np.array([])

        joint_frame = pd.merge_asof(
            state_frame,
            reward_frame,
            on='time',
            by='name',
            tolerance=pd.Timedelta('1H')
        )

        joint_frame = pd.merge_asof(
            joint_frame,
            action_frame,
            on='time',
            by='name',
            tolerance=pd.Timedelta('1H')
        )
        joint_frame = joint_frame.dropna()
        joint_frame = joint_frame.drop(columns=['name'])
        if len(joint_frame) == 0:
            return joint_frame

        # logger.info(joint_frame)
        return joint_frame

    def step_loss(self) -> np.ndarray:
        """Step Loss

        The array to allow agent to fit to loss.

        Returns
        -------
        np.ndarray
            Array for y real vs prediction values.
        """

        self.indexer.loss()

        reward_pred_frame = self.reward_pred_frame()
        reward_frame = self.reward_frame()
        if reward_pred_frame is None or reward_frame is None:
            # Create rules to skip the rest of the logic
            return self.get_transform_log()
            # logger.success(reward_pred_frame)
        joint = pd.merge_asof(
            reward_pred_frame,
            reward_frame,
            on='time',
            by='name',
            tolerance=pd.Timedelta('1H')
        )
        joint = joint.dropna()
        joint = joint.drop(columns=['name'])
        # logger.debug("\n{joint}", joint=joint)
        return self.get_transform_log()

    def step_pred(self) -> np.ndarray:
        """Step Pred

        The array to allow agent to predict the next reward.

        Returns
        -------
        np.ndarray
            Array for y real vs prediction values.
        """

        self.indexer.pred()

        state_frame = self.state_frame()
        action_frame = self.action_frame()
        if state_frame is None and action_frame is None:
            # Create rules to skip the rest of the logic
            return np.array([])
        elif state_frame is not None and action_frame is None:
            return state_frame
        print("\n")
        logger.debug(state_frame)
        logger.success(action_frame)
        print("\n")
        joint = pd.merge_asof(
            state_frame,
            action_frame,
            on='time',
            by='name',
            tolerance=pd.Timedelta('1H')
        )

        return joint

    def step_log_pred(self, prediction: float):
        """Step Input

        Save the reward predictions from the gassian process to the database.

        Parameters
        ----------
        prediction : float
            The prediction from the agent.
        """
        # TODO: Figure out how to save this into the future instead. Last part.
        self.indexer.pred()
        _reward = self.indexer.reward
        dist = _reward.y_target_head
        _timestamp = self.time.peak_num(dist)
        self.save_estimation(prediction, _timestamp=_timestamp)

    def step_log_returns(self, reward: float):
        """Step Input

        Save the reward predictions from the gassian process to the database.

        Parameters
        ----------
        prediction : float
            The prediction from the agent.
        """
        self.save_reward(reward)