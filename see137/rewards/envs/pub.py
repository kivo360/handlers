from see137.importers import *
from see137.handlers.states.envs.priv import __PrivateEnv
from enum import Enum


class ResponseType(Enum):
    TRAIN = 1
    PREDICT = 2
    LOSS = 3


class StateEnv(__PrivateEnv):

    def __init__(self, prediction_lag: int = 3, window_size: int = 1):
        """
            Use to determine exactly what you should reset at the beginning.
        """
        super().__init__(prediction_lag=prediction_lag, window_size=window_size)

    def step(self, response: ResponseType = ResponseType.PREDICT) -> np.array:
        """State Start

        Get information to make a proper decision in the exchange class.

        Returns
        -------
        np.array
            Exchange
        """
        self.get_transform_log()
        res = None
        if response == ResponseType.TRAIN:
            res = self.step_train()
        elif response == ResponseType.PREDICT:
            res = self.step_pred()
        elif response == ResponseType.LOSS:
            res = self.step_loss()
        return res

    def step_done(self, predictions: float) -> None:
        """Step Done

        Save the predictions from the gassian process to the database.

        Parameters
        ----------
        prediction : float
            The prediction from the agent.
        """

    def reset(self):
        self.time.reset()
        self.reset_technical_analysis()