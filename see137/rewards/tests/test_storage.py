from loguru import logger
from see137.rewards.schemas.placeholder import RewardModel
from see137.rewards import actions as ractions

def test_extract_reward_system_params():
    logger.warning("Initialize reward then extract the parameters.")
    logger.info("Initalize reward system")
    assert True, "Wasn't able to initialize the reward system."
    logger.info("Check that the reward system has parameters.")
    assert True, "The reward system didn't have parameters."
    logger.info("Extract the parameters ... parameters is not None")
    logger.debug("Initialize empty reward system.")
    
    logger.debug("Send parameters into empty reward system")
    logger.critical("Check that the reward system has parameters.")
    assert True, "Wasn't able to properly extract file."


def test_save_system(stp_actions:ractions.ActionsRewardSystem):
    # actions = ActionsRewardSystem()
    _first = stp_actions.load_state_blob()
    assert isinstance(_first, RewardModel), "The initially loaded item is not the reward system"