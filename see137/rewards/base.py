from typing import Any, Dict, Optional

from darwin_ml.handlers import (
    IncrementalEngine,
    TAPriceData,
)

from darwin_ml.models import (
    StateSpaceModel,
)
from jamboree.handlers.abstracted.datasets import PriceData
from jamboree.handlers.complex.backtestable.db import (
    BacktestDBHandlerWithAccess
)

from see137.handlers.exchange import PortfolioHandler
from see137.rewards.schemas import dantic, placeholder

__all__ = ['BaseRewardEnv']


class BaseRewardEnv(BacktestDBHandlerWithAccess):
    """
        # StateSpace

        This is a direct copy over of the state space.

        The general goal of this class is to ensure we can adapt to the proper elements of the end to end testing suite.

        We do the following:

        1. Load/Store the online learning models
        2. Get the appropiate data to make predictions and changes on.
        3. Record predictions and recent changes.
        4. Return the states space for immediate consumption.

    """

    def __init__(self, lag: int = 5):
        super().__init__()
        # Entity Variables
        self.entity = "reward_system"
        self.submetatype = "generalizable"
        self.required = {
            "name": str,
            "category": str,
            "subcategories": dict,
            "metatype": str,
            "submetatype": str,
            "abbreviation": str
        }
        self.metatype = self.entity
        self.lag = lag
        self.__is_mock: bool = False

        # User asset variables
        self.__user: Optional[str] = None
        self.__exchange: str = "binance"
        self.__asset_dict: Optional[dantic.AssetDict] = None

        self._incremental_engine: IncrementalEngine = IncrementalEngine()
        self._space: placeholder.RewardModel = placeholder.RewardModel(
            prediction_lag=self.lag
        )
        self._price: PriceData = PriceData()
        self._technical = TAPriceData()
        self._incremental_engine.blobfile = self._space
        self._portfolio: PortfolioHandler = PortfolioHandler()
        self.__is_mock_portfolio = False

    @property
    def asset_dict(self) -> dantic.AssetDict:
        if self.__asset_dict is None:
            raise AttributeError(
                "Asset dictionary, used to identify the sepecific reward values, "
            )
        return self.__asset_dict

    @asset_dict.setter
    def asset_dict(self, _asset_dict: Dict[str, Any]):
        self.__asset_dict = dantic.AssetDict(**_asset_dict)

    @property
    def current_time(self) -> float:
        return self.time.head

    @property
    def user_id(self) -> str:
        if self.__user is None:
            raise AttributeError("User needs to be set.")
        return self.__user

    @user_id.setter
    def user_id(self, user_id: str):
        self.__user = user_id
        self['user_id'] = self.__exchange

    @property
    def exchange(self) -> str:
        return self.__exchange

    @exchange.setter
    def exchange(self, exchange: str):
        self.__exchange = exchange
        self['exchange'] = self.__exchange

    @property
    def is_mock(self) -> bool:
        return self.__is_mock

    @is_mock.setter
    def is_mock(self, __is_mock: bool):
        self.__is_mock = __is_mock

    @property
    def is_mock_portfolio(self) -> bool:
        return self.__is_mock_portfolio

    @is_mock_portfolio.setter
    def is_mock_portfolio(self, __is_mock: bool):
        self.__is_mock_portfolio = __is_mock

    @property
    def episode(self) -> str:
        return self._episode

    @episode.setter
    def episode(self, _episode: str):
        self._episode = _episode
        self['episode'] = _episode