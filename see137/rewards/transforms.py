from loguru import logger
from see137.rewards import actions
from darwin_ml.technical import fiboacci_general


class RewardSystemTransforms(actions.ActionsRewardSystem):
    """State Space Container

    Properties to get containers representing the current properties of the user price pair.

    Parameters
    ----------
    base : [type]
        [description]
    """

    def __init__(self, prediction_lag: int = 5):
        super().__init__(prediction_lag)

    @property
    def is_reset_mock(self) -> bool:
        if not self.is_mock:
            return False

        if self.technicals.count() > 0:
            return False
        return True

    @property
    def is_step_mock(self) -> bool:
        ''' Determines if we're going to generate technical analysis '''
        if not self.is_mock:
            return True

        if self.technicals.count() > 0:
            return True

        return False

    def preprocess_all_existing_price_info(self):
        df = self.price.dataframe_all()
        ta_features = fiboacci_general(
            df,
            target="close",
            high="high",
            _open="open",
            low="low",
            volume="volume",
            adj_close="adj_close"
        )
        self.technicals.reset()
        self.technicals.store_time_df(ta_features)

    def preprocess_limited_price_info(self):
        # curr_time = self.current_time
        df = self.price.dataframe_from_head()
        ta_features = fiboacci_general(
            df,
            target="close",
            high="high",
            _open="open",
            low="low",
            volume="volume",
            adj_close="adj_close"
        )
        self.technicals.reset()
        self.technicals.store_time_df(ta_features)

    def reset_technical_analysis(self):
        if self.is_reset_mock:
            logger.debug("RESETTING TECHNICAL ANALYSIS")
            self.preprocess_all_existing_price_info()
            return
        elif self.is_mock == False:
            self.preprocess_limited_price_info()
