# `manifold-rl` Save Structure

**The reward function is simply the mechanism of storing a model and its parameters.**

A reward function will likely have a class instantiation at the very beginning of a test run and will have a large set of parameters. This reward function is `manifold-rl`. One will have a few things to keep in mind. Before going into the gritty details I'll state one clear fact. There will be a lot of parameters/variables to track and optimize. So far, there are 2 key layers of variables to consider:

## Layers of Parameters/Variables

1. *The Highest Level - Hyperparameters to max out profit and minimize risk.*
    1. There will be high level variables that we don't want an upper limit. If we can get infinite growth in these variables combined we win.
        1. This would be a dynamic balance between profit and sortino ratio in risk.
        1. Ultimately we're trying to max out these variables at a fast rate.
            1. E.g. This means cutting down forms of risk (adaptive scalarization) while maxing out potential profit.
        1. This is what one would call a general set of variables we'd try to optimize for.
        1. We'd be optimizing other models using a population-based training approach.
            1. This involves setting up live trials.
            1. We'll likely have to manually setup the slices for these live trials and run them through using our own logic. I'll run through this a bit later.
                1. It will likely come after the basic movement control is finished.
                1. NOTE: This will be really complex. Will likely need to create a separate project for this.
        1. The variables we're optimizing here would be:
            1. The momentum parameter for the movement rules.
            1. Safety constraints/mechanisms
                1. Reachability analysis sensitivity
                    1. The worse-case tolerance relative to some other variables.
            1. Horizon of estimations range.
                1. Used inside of lower layer to determine where safety points are and are not.
                1. Get worse-case trajectory (estimation using DeepGP) with noise broadcasted in the state array.
            1. Adaptive Scalarization
                1. We'd figure out what is the best scalarization distribution between the variables we're watching.
                    1. This is the secret sause.
1. *Low-layer - Movement Rules + Safety Constraints + Adaptive Scalarization*
    1. Basic rules for the entire system.
        1. Initially desired behavior given a certain state.
        1. Movement Rules (velocity/acceleration).
        1. Objective Importance Rules.
        1. Scalarization weights.
            1. Importance rules are different from scalarization weights. One determines change by state.
            1. It should be 100% upfront.
    1. Initial optimization of desired points.
        1. We'd create an ODE on initalization

```python
from manifold import Manifold
from see137.rewards import RewardEnv
import typing
# Declaring the reward function.
reward_machine: Manifold = Manifold(...)

# This will optimize the points to ODEs to return rewards given states.
reward_machine.reset()

step_dict: dict = {
    "monitored": {
        "price": random.uniform(0, 100),
        # ...
    }

}

# Steps through
reward_machine.step(step_dict)

# Get's the state of the reward
parameter_state_dict:typing.Dict[typing.AnyStr, typing.Any] = reward_machine.state_dict()

# The state_dict
meta_state_dict: typing.Dict[typing.AnyStr, typing.Any] = parameter_state_dict['meta']
dim_state_dict: typing.Dict[typing.AnyStr, typing.Any]  = parameter_state_dict['dimension']

# Extract state dicts
meta_state_dict: typing.Dict[typing.AnyStr, typing.Any]    = reward_machine.meta_state_dict()
dim_state_dict: typing.Dict[typing.AnyStr, typing.Any]     = reward_machine.dim_state_dict()

# Load states into the reward machine
reward_machine.load_state_dict(meta_state_dict, category="meta")
reward_machine.load_state_dict(dim_state_dict, category="dimension")

# Set everything here
reward_machine.load_state_dict(parameter_state_dict)
```

## State Transfer For The Reward

Given the design above, I'll need to run through the following parts to:

1. Set asset parameters.
1. Get the general reward model if it's changed.
    1. We'll store the model locally if it's updated.
    1. This is a note for stream processing (redis-streams) and local storage () in the future.
    1. Will use the combination of the two methods, plus lazy dags (thanks weld-rs), to reduce computational overhead in version 2.0.
1. Get the contextual variables to estimate the reward inside of a dictionary.
1. Get Reward
    1. Bundle them based on each criteria set and pass sectioned off dictionaries to their specific problem set.
    1. Create tensor to train on as well (in the background).
    1. Run through estimations and adaptation steps here.
        1. **Note:** this will seriously have to be done in the background, on a cluster or something.
1. Send task to background to save the parameters.
    1. Threads, please oh threads, actors and kafka. This will kill the system if it were in sycnhronous programming.
    2. Create a synchronous function first `save_parameters` then optimize. You have plenty you plan on doing Kevin ... need to get done. Not right now.
        1. On a lower level note
1. Send reward to rllib.
1. Pass the asset on so it can go through another round.


Given what is said the code base should look like the abstract class below. We're doing abstracts to define functionality first.



```python

```


