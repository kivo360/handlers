from jamboree import Jamboree
from see137.importers import *
from .envs.pub import StateEnv, ResponseType


class AsyncRewardSystem:

    def __init__(self, host: str = "localhost", port: int = 6379) -> None:
        self._reward_system = StateEnv()
        self._reward_system.processor = Jamboree(
            REDIS_HOST=host, REDIS_PORT=port
        )

    def set_parameters(self, **kwargs):
        """ 
            # Set Parameters
        
            An asynchronus wrapper around the reward system. The reward system
        """
        user_id = kwargs.get('user_id')
        category = kwargs.get('category')
        subcategories = kwargs.get('subcategories', {})
        abbreviation = kwargs.get('abbreviation')
        episode = kwargs.get('episode')
        asset_dict = kwargs.get('asset_dict', {})
        is_live = kwargs.get('live', False)
        is_mock = kwargs.get('is_mock', True)
        is_mock_portfolio = kwargs.get('is_mock_portfolio', True)
        symbol = asset_dict.get("name")

        user_id = str(user_id)
        category = str(category)
        abbreviation = str(abbreviation)
        episode = str(episode)
        asset_dict = asset_dict
        is_live = is_live
        is_mock = is_mock
        is_mock_portfolio = is_mock_portfolio
        self._reward_system.name = str(symbol)
        self._reward_system.abbreviation = abbreviation
        self._reward_system.category = category
        self._reward_system.subcategories = subcategories
        self._reward_system.user_id = user_id
        self._reward_system.episode = episode
        self._reward_system.asset_dict = asset_dict
        self._reward_system.live = is_live
        self._reward_system.is_mock = is_mock
        self._reward_system.is_mock_portfolio = is_mock_portfolio

    def step(
        self,
        response: ResponseType = ResponseType.PREDICT,
        **kwargs
    ) -> np.array:
        self.set_parameters(**kwargs)
        self.reset()
        result = self._reward_system.step(response)

        return result

    def log_prediction(self, prediction: float, **kwargs):
        self.set_parameters(**kwargs)
        self.reset()
        self._reward_system.step_log_pred(prediction)

    def log_returns(self, returns: float, **kwargs):
        self.set_parameters(**kwargs)
        self.reset()
        self._reward_system.step_log_returns(returns)

    def log_action(self, action, **kwargs):
        self.set_parameters(**kwargs)
        self.reset()
        self._reward_system.save_action(action)

    def reset(self):
        self._reward_system.reset()