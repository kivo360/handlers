from loguru import logger
from darwin_ml.middleware import containers

from see137.rewards import transforms


class RewardSystemContainers(transforms.RewardSystemTransforms):
    """Reward System Containers

    Properties to get containers representing the current properties of the user price pair.

    Parameters
    ----------
    base : [type]
        [description]
    """

    def __init__(self, prediction_lag: int = 5):
        super().__init__(prediction_lag)

    @property
    def price_container(self) -> containers.PriceContainer:
        current_price_set = (self.price.closest_head_omitted())
        current_price_set_back = (
            self.price.closest_peakback_by_omitted(self.lag)
        )

        price_container = containers.PriceContainer(
            current_price_set, current_price_set_back
        )
        return price_container

    @property
    def feature_container(self) -> containers.FeaturesContainer:
        current_feature_set = self.technicals.closest_head_omitted()
        current_feature_set_back = (
            self.technicals.closest_peakback_by_omitted(self.lag)
        )
        feature_container = containers.FeaturesContainer(
            current_feature_set, current_feature_set_back
        )
        return feature_container

    @property
    def allocation_container(self) -> containers.AllocationContainer:
        abbv = self.abbreviation
        allocation = self.portfolio.current_allocation(
            symbol=abbv,
            is_mocked=self.is_mock_portfolio,
        )
        fill_pct = self.portfolio.current_pct_worth(
            symbol=abbv,
            is_mocked=self.is_mock_portfolio,
        )
        allocation_container = containers.AllocationContainer(
            allocation, fill_pct
        )
        return allocation_container
