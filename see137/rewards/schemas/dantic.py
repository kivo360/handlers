import uuid
from pydantic import BaseModel, Field
from faker import Faker

fake = Faker()


class SubcategoryDict(BaseModel):
    market: str = Field(default_factory=fake.cryptocurrency_name)


class AssetDict(BaseModel):
    name: str = Field(default_factory=fake.cryptocurrency_name)
    category: str = "markets"
    subcategories: dict = Field(default_factory=SubcategoryDict().dict)
    submetatype: str = "GEN"
    abbreviation: str = Field(default_factory=fake.cryptocurrency_code)
    live: bool = False
    episode: str = Field(default_factory=lambda: uuid.uuid4().hex)