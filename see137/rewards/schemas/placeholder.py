import numpy as np
from darwin_ml.models import AllocationStepSystem
from darwin_ml.models import OnlineModels
from darwin_ml.models import PriceDeltaModel
from darwin_ml.models.creme_ml.transforms import FeatureWindow
from darwin_ml.middleware import containers
# from jamboree.utils.context import timecontext


class RewardModel(object):
    """Blob Storage Reward Model

        Manifold is supposed to go here.
    """

    def __init__(self, prediction_lag: int = 5):
        # Get the change of price over the given lag period
        self.price_delta_calc: PriceDeltaModel = PriceDeltaModel()
        # Get the statistical change for the allocations over time.
        self.allocation_step: AllocationStepSystem = AllocationStepSystem()
        # Predict and get a stacked version of the predictions over time.
        self.prediction_step: OnlineModels = OnlineModels()
        # Use this to locally store features and determine features relavent to the predictions.
        self.feature_window: FeatureWindow = FeatureWindow(lag=prediction_lag)

        self.prediction_step.set_lag(lag=prediction_lag)
        self.price_delta_calc.set_lag(lag=prediction_lag)

        self.current_prev_feature = None

    @property
    def is_previous_feature(self) -> bool:
        self.current_prev_feature = self.feature_window.get()
        return bool(self.current_prev_feature)

    def step(
        self,
        all_c: containers.AllocationContainer,
        feature_container: containers.FeaturesContainer,
        price_container: containers.PriceContainer
    ):

        current = feature_container.current

        self.feature_window.update(current)
        if not self.is_previous_feature:
            return None

        delta = self.price_delta_calc.step(price_container)
        prediction_model = self.prediction_step.step(feature_container, delta)
        allocation_model = self.allocation_step.step(
            all_c.percentage, all_c.filling
        )
        joined_space = np.concatenate([
            prediction_model.space(), allocation_model.space()
        ])
        joint_dict = prediction_model.dict()
        joint_dict.update(allocation_model.dict())
        return joined_space, joint_dict