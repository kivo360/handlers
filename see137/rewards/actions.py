from see137.importers import *
from darwin_ml.models import StateSpaceModel
from jamboree.handlers.abstracted.datasets import PriceData

# from see137.handlers.states import base, props
from see137.rewards import base, props
from see137.rewards.schemas import dantic, placeholder

# NOTE: Consider the gloabl share options when doing refactor. Do a test project to know coding format.


class __ActionsRewardSystem(props.RewardWithProperties):

    def __init__(self, prediction_lag: int = 3):
        """Private Actions State Space 

        A set of private functions for the state space.

        Parameters
        ----------
        prediction_lag : int, optional
            Number of steps behind we're recording, by default 3
        """
        super().__init__(prediction_lag)

    def __init_default_test(self):
        super().time.reset()
        super().time.change_stepsize(hours=0, days=5)
        self.is_mock = True

    def __init_default_dict(self):
        asset = dantic.AssetDict().dict()
        self.asset_dict = asset

    def __init_default_variables_dict(self):
        self.name = self.asset_dict.name
        self.category = self.asset_dict.category
        self.subcategories = self.asset_dict.subcategories
        self.submetatype = self.asset_dict.submetatype
        self.abbreviation = self.asset_dict.abbreviation
        self.live = self.asset_dict.live
        self.episode = self.asset_dict.episode

    @property
    def is_blob(self) -> bool:
        """Is Blob File

            Checks if there's a blob file inside of the incremental storage.
        """
        _ = super()
        curr = (_.incremental.blobfile)

        return curr is not None

    @property
    def is_current(self) -> bool:
        """Is Current Model

            Checks if there's a model inside of current strategies
        """
        _ = super()
        curr = self.incremental.current_strategy

        return curr is not None

    def __many_times(self, alt={}):
        return [
            str(maya.MayaDT(x['time']))
            for x in self.many(ar="relative", alt=alt)
        ]

    def __maya_time(self):
        return str(maya.MayaDT(self.current_time))

    def __last_by_rel(self, alt={}):
        _time = self.current_time
        return super().last_by(_time, ar="relative", alt=alt)

    def __last_by_rel_peak(self, peak: int = 1, alt={}):
        abs_peak = abs(peak)
        if peak > 0:
            _time = self.time.peak_num((abs_peak + 1))
        elif peak < 0:
            _time = self.time.peak_back_num((abs_peak + 1))
        return super().last_by(_time, ar="relative", alt=alt)

    def __last_by_rel_peak_range(self,
                                    peak: int = 1,
                                    start: int = 0,
                                    alt={}):
        _initial_time: float = 0.0
        _time_peak = 0.0
        _istart = abs(start)
        _ipeak = abs(peak)

        if start > 0:
            _initial_time = self.time.peak_num(_istart)
        elif start < 0:
            _initial_time = self.time.peak_back_num((_istart))
        else:
            _initial_time = self.current_time

        if peak > 0:
            _time_peak = self.time.peak_num(_ipeak)
        elif peak < 0:
            _time_peak = self.time.peak_back_num((_ipeak))
        else:
            _time_peak = self.current_time

        return super().in_between(
            _time_peak,
            _initial_time,
            ar="relative",
            alt=alt,
        )


ATTR_CATCH = logger.opt(raw=True, exception=False).catch(
    exception=AttributeError,
    message="Model missing...",
)


class ActionsRewardSystem(__ActionsRewardSystem, props.RewardWithProperties):

    def __init__(self, prediction_lag: int = 3):
        super().__init__(prediction_lag)
        self.act_detail = {
            "detail": "actions"
        }
        self.std_detail = {
            "detail": "state_space"
        }
        self.rwd_detail = {
            "detail": "reward"
        }
        # Store Y results and estimations
        self.est_detail = {
            "detail": "estimation"
        }

        self.result_detail = {
            "detail": "result"
        }

    def init_default(self):
        logger.info("Loading Default Values To Test On")
        self.__init_default_dict()
        self.__init_default_variables_dict()
        self.__init_default_test()

    def get_detail(self, name: str, value: Any) -> Dict[str, Any]:
        _dict_json = {
            name: value
        }
        _dict_json.update(self.update_dict)

        return _dict_json

    def get_count(self, name: str = ""):
        if bool(name):
            # logger.debug(self.episode)
            return self.count(alt={"detail": name})
        return self.count()

    def update_step(self, is_asset: bool = False):
        alt = {
            "detail": "step"
        }
        key = {
            "key": uuid.uuid4().hex
        }
        if is_asset:
            alt.update(self.asset_dict.dict())
            return super().save(key, alt=alt)
        return super().save(key, alt=alt)

    def get_step_count(self, is_asset: bool = False) -> int:
        alt = {
            "detail": "step"
        }
        if is_asset:
            alt.update(self.asset_dict.dict())
            return super().count(alt=alt)
        return super().count(alt=alt)

    #--------------------------------------------------------#
    #--------------------------------------------------------#
    #                  Blob State Manipulation               #
    #--------------------------------------------------------#
    #--------------------------------------------------------#

    def pull_model(self,
                    update_lag: bool = False
                   ) -> Optional[placeholder.RewardModel]:
        _model = None
        _ = super()
        with ATTR_CATCH:
            _model = _.incremental.models
            if update_lag:
                clag = _.__dict__.get('lag', 3)
                _model.prediction_step.set_lag(clag)
                _model.price_delta_calc.set_lag(clag)
        return _model

    def load_state_blob(self) -> Optional[placeholder.RewardModel]:
        """Load State Blob
        
        Saves the blob then pulls the model from the database. Guarunteed to not have confusions.
        """
        logger.warning("Extracting model parameters from system.")
        super().incremental.reset()
        return self.pull_model()

    def clear_state_blob(self) -> None:
        super().incremental.current_strategy = None

    def save_state_blob(self, update_lag: bool = False):
        logger.debug("Saving model")
        _ = super()
        if _.is_current:
            _model: Optional[placeholder.RewardModel
                             ] = self.pull_model(update_lag=update_lag)

            _.incremental.save_file(_model)
        elif _.is_blob:
            self.load_state_blob()
        else:
            _.incremental.blobfile = placeholder.RewardModel(
                prediction_lag=self.lag
            )
            _.incremental.reset()

    def save_and_clear(self):
        self.save_state_blob()
        self.clear_state_blob()

    #--------------------------------------------------------#
    #--------------------------------------------------------#
    #               StateSpaceModel Information              #
    #--------------------------------------------------------#
    #--------------------------------------------------------#

    def get_prediction_step_lag(self) -> int:
        _pulled: placeholder.RewardModel = self.pull_model()
        return _pulled.price_delta_calc.pct_model.lag

    #--------------------------------------------------------#
    #--------------------------------------------------------#
    #               Save State Actions                       #
    #--------------------------------------------------------#
    #--------------------------------------------------------#

    # ------------#
    #    Saves    #
    # ------------#

    def save_action(self, _action: np.array) -> None:
        _action_json = self.get_detail(
            "action",
            _action,
        )
        alt = self.act_detail
        super().save(_action_json, alt=alt)
        return _action_json

    def save_state(self, _state: np.array):
        _state_json = self.get_detail("state_space", _state)
        alt = self.std_detail
        self.save(_state_json, alt=alt)
        return _state_json

    def save_reward(self, _reward: float):
        _reward_json = self.get_detail("reward", _reward)
        alt = self.rwd_detail
        super().save(_reward_json, alt=alt)
        return _reward_json

    def save_estimation(
        self,
        _estimation: Union[float, int],
        _timestamp: Optional[float] = None
    ):
        _est_json = self.get_detail("estimation", _estimation)
        alt = self.est_detail
        if _timestamp is not None:
            _est_json['ptime'] = _timestamp
        super().save(_est_json, alt=alt)
        return _est_json

    def save_result(self, _result: Union[float, int]):
        _result_json = self.get_detail("result", _result)
        alt = self.result_detail
        super().save(_result_json, alt=alt)
        return _result_json

    # ------------#
    #    Loads    #
    # ------------#

    def load_action(self,
                    n: int = 0) -> Union[np.array, List[Union[float, int]]]:
        if n == 0:
            last_action = self.__last_by_rel(alt=self.act_detail)
            _act_vals = last_action.get("action")
            return np.array(_act_vals)

        last_action = self.__last_by_rel_peak(n, alt=self.act_detail)
        return np.array(last_action.get("action"))

    def load_state(self,
                    n: int = 0
                   ) -> Union[np.array, List[List[Union[float, int]]]]:
        if n == 0:
            last_state = self.__last_by_rel(alt=self.std_detail)
            return np.array(last_state.get("state_space"))

        last_state = self.__last_by_rel_peak(n, alt=self.std_detail)
        return np.array(last_state.get("state_space"))

    def load_reward(self, n: int = 0) -> float:
        if n == 0:
            last_state = self.__last_by_rel(alt=self.rwd_detail)
            return round(float(last_state.get("reward")), 4)

        last_state = self.__last_by_rel_peak(n, alt=self.rwd_detail)
        return round(float(last_state.get("reward")), 4)

    def load_estimation(self, n: int = 0) -> float:
        if n == 0:
            last_state = self.__last_by_rel(alt=self.est_detail)
            return int(last_state.get("estimation"))

        last_state = self.__last_by_rel_peak(n, alt=self.est_detail)
        return int(last_state.get("estimation"))

    def load_result(self, n: int = 0) -> int:
        """Load Change Results

        Load the results of reward changes as time progresses. 

        Parameters
        ----------
        n : int, optional
            Number of time steps back, by default 1

        Returns
        -------
        float
            the result.
        """
        if n == 0:
            last_state = self.__last_by_rel(alt=self.result_detail)
            return int(last_state.get("result"))

        last_state = self.__last_by_rel_peak(n, alt=self.result_detail)
        return int(last_state.get("result"))

    def load_reward_set(self, num: int = -2, start: int = 0):
        """Load A Set Of Rewards

        Load multiple rewards at once from the current time.

        Parameters
        ----------
        num : int, optional
            Number of lookback points, by default 2
        """

        return self.__last_by_rel_peak_range(
            peak=num,
            start=start,
            alt=self.rwd_detail,
        )

    def load_action_set(self, num: int = 2, start: int = 0):
        """Load A Set Of Rewards

        Load multiple rewards at once from the current time.

        Parameters
        ----------
        num : int, optional
            Number of lookback points, by default 2
        """

        return self.__last_by_rel_peak_range(
            peak=num,
            start=start,
            alt=self.act_detail,
        )

    def load_state_set(self, num: int = 2, start: int = 0):
        """Load A Set Of Rewards

        Load multiple states at once from the current time.

        Parameters
        ----------
        num : int, optional
            Number of lookback points, by default 2
        """

        return self.__last_by_rel_peak_range(
            peak=num,
            start=start,
            alt=self.std_detail,
        )

    def load_estimation_set(self, num: int = 2, start: int = 0):
        """Load A Set Of Rewards

        Load multiple states at once from the current time.

        Parameters
        ----------
        num : int, optional
            Number of lookback points, by default 2
        """

        return self.__last_by_rel_peak_range(
            peak=num,
            start=start,
            alt=self.est_detail,
        )

    def load_result_set(self, num: int = 2, start: int = 0) -> int:
        """Load A Set Of Rewards

        Load multiple states at once from the current time.

        Parameters
        ----------
        num : int, optional
            Number of lookback points, by default 2
        """

        return self.__last_by_rel_peak_range(
            peak=num,
            start=start,
            alt=self.std_detail,
        )

    def set_lag(self, lag: int):
        pass