from see137.importers import *
from see137.rewards import contain
from see137.rewards.schemas import placeholder
from multiprocessing import cpu_count
from concurrent.futures import ThreadPoolExecutor
CPU_NUM = cpu_count()
POOL_COUNT = int(CPU_NUM * 1.4)


class RewardSystemMachine(contain.RewardSystemContainers):
    """State Space Container

    Properties to get containers representing the current properties of the user price pair.

    Parameters
    ----------
    base : [type]
        [description]
    """

    def __init__(self, prediction_lag: int = 3):
        """
            TODO: Add get and set(s) estimate functions. 
            Also get_before() function to better allow for better dataframe alignment
        """

        super().__init__(prediction_lag)

        self.pool = ThreadPoolExecutor(max_workers=POOL_COUNT)

    def __enter__(self) -> placeholder.RewardModel:
        loaded_state = self.load_state_blob()
        if loaded_state is None:
            raise AttributeError("Loaded Reward System Doesn't Exist")
        return loaded_state

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.pool.submit(self.save_and_clear)

    def get_transformed(self) -> np.ndarray:
        ac = self.allocation_container
        pc = self.price_container
        fc = self.feature_container
        transformed = np.array([])

        with self as online:
            transformed = online.step(
                all_c=ac,
                feature_container=fc,
                price_container=pc,
            )
        return transformed

    def get_transform_log(self) -> np.ndarray:
        # TODO: Remember to change state save to show more variables
        transformed_np = self.get_transformed()
        if transformed_np is not None:
            super().save_state(transformed_np[0])
            return transformed_np[0]
        return transformed_np

    def state_count(self) -> int:
        return self.get_count("state_space")

    def reward_count(self) -> int:
        return self.get_count("reward")

    def action_count(self) -> int:
        return self.get_count("actions")

    def estimation_count(self) -> int:
        return self.get_count("estimation")