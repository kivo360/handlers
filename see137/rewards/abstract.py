import abc
import threading
from concurrent.futures import ThreadPoolExecutor


class RewardEnv(abc.ABC):

    def __init__(self) -> None:
        self.local = threading.local()
        self.local.executor = ThreadPoolExecutor()

    def load_parameters(self):
        raise NotImplementedError("Model specific parameters.")

    def get_hyperparameters(self):
        raise NotImplementedError("Get the general model.")

    def get_context(self) -> dict:
        raise NotImplementedError("Getting all context variables here.")

    def get_reward(self) -> float:
        raise NotImplementedError("Need to get the reward.")

    def save_parameters(self) -> dict:
        raise NotImplementedError("Saving specific items parameters.")