#!/usr/bin/env python
"""Example of training with a policy server. Copy this file for your use case.

To try this out, in two separate shells run:
    $ python cartpole_server.py --run=[PPO|DQN]
    $ python cartpole_client.py --inference-mode=local|remote

Local inference mode offloads inference to the client for better performance.
"""

import argparse
import gym
import asyncio
from loguru import logger
from see137.reinforcement import aclient, agym
parser = argparse.ArgumentParser()
parser.add_argument(
    "--no-train", action="store_true", help="Whether to disable training."
)
# parser.add_argument(
#     "--inference-mode", type=str, required=True, choices=["local", "remote"]
# )
parser.add_argument(
    "--off-policy",
    action="store_true",
    help="Whether to take random instead of on-policy actions."
)
parser.add_argument(
    "--stop-reward",
    type=int,
    default=9999,
    help="Stop once the specified reward is reached."
)


async def ainfinite():
    index = 0
    while True:
        index += 1
        yield index


async def mainloop(args):
    env = gym.make("CartPole-v0")
    aenv = agym.AsyncGym(env)
    client = aclient.AsyncPolicyClient()

    eid = await client.start_episode()
    obs = await aenv.reset()
    rewards = 0

    while True:
        if args.off_policy:
            action = await aenv.action_sample()
            await client.log_action(eid, obs, action)
        else:
            action = await client.get_action(eid, obs)
        obs, reward, done, info = await aenv.step(action)
        rewards += reward
        await client.log_returns(eid, reward, info=info)
        if done:
            logger.info("Total reward:", rewards)
            if rewards >= args.stop_reward:
                print("Target reward achieved, exiting")
                exit(0)
            rewards = 0
            await client.end_episode(eid, obs)
            obs = await aenv.reset()
            eid = await client.start_episode(training_enabled=not args.no_train)


if __name__ == "__main__":
    args = parser.parse_args()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(mainloop(args))
    finally:
        loop.close()
        asyncio.set_event_loop(None)