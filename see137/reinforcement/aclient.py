import uuid
from typing import Any, Optional, Union
from loguru import logger
from ray.rllib.env.policy_client import PolicyClient


class AsyncPolicyClient:

    def __init__(
        self,
        host: str = "localhost",
        port: int = 9900,
        inference_mode: str = "local",
        secure: bool = False,
        interval: int = 5
    ) -> None:
        url = self.__get_url(host=host, port=port, secure=secure)
        logger.warning(url)
        self.client: PolicyClient = PolicyClient(
            url,
            inference_mode=inference_mode,
            update_interval=interval,
        )

    def __get_url(
        self,
        host: str = "localhost",
        port: int = 9900,
        secure: bool = False,
    ):
        protocol = self.__get_protocol(secure=secure)
        url = f"{protocol}://{host}:{port}"
        return url

    def __get_protocol(self, secure: bool = False):
        return {
            False: "http", True: "https"
        }[secure]

    async def start_episode(
        self, episode_id: Optional[str] = None, training_enabled: bool = True
    ):
        return self.client.start_episode(
            episode_id=episode_id,
            training_enabled=training_enabled,
        )

    async def end_episode(self, eid: str, obs: Any):
        self.client.end_episode(eid, obs)

    async def log_action(self, eid: str, obs, action):
        self.client.log_action(eid, obs, action)

    async def get_action(self, eid: str, obs):
        action = self.client.get_action(eid, obs)
        return action

    async def log_returns(
        self, eid: str, reward: Union[float, int], info: dict = {}
    ):
        self.client.log_returns(eid, reward, info=info)
