from loguru import logger
from see137.importers.testing.asyncrl import *


def check_server(host: str = "localhost", port: int = 9900) -> bool:
    socket_checker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    location = (host, port)
    result_of_check = socket_checker.connect_ex(location)
    _result_of_check = not bool(result_of_check)
    socket_checker.close()
    return _result_of_check


@pytest.mark.dependency(name="start_server")
@pytest.mark.asyncio
async def test_start_server(xprocess, server_path):
    """Start Server Run

    Test That We're Starting The Server.
    """
    if check_server():
        # The server is already open.
        logger.success("Server already started")
        assert True, "The server is not open."
        return

    server_str = str(server_path)
    logger.error(server_str)
    assert server_path.exists(), "Server doesn't exist for some reason."
    assert server_str is not None, "The path doesn't exist for some reason."
    try:
        xprocess.getinfo("rl_server").terminate()
    except Exception as e:
        logger.exception(e)

    xprocess.ensure(
        "rl_server",
        lambda cwd: ("policy_server_input.py:91", [sys.executable, server_str])
    )
    assert check_server(), "The server hasn't been started"


@pytest.mark.dependency(name="new_episode", depends=["start_server"])
@pytest.mark.asyncio
async def test_new_episode():
    assert check_server(), "Server hasn't started"
    logger.info("Accessing a new episode.")
    async_client = aclient.AsyncPolicyClient(
        host="localhost",
        port=9900,
        secure=False,
    )
    try:
        episode_id: str = await async_client.start_episode()
    except Exception as e:
        logger.exception(e)
        assert False, "We weren't able to get an episode id"

    assert episode_id is not None, "The episode doesn't exist"
    assert isinstance(episode_id, str), "The episode_id isn't a string"

    await async_client.start_episode(episode_id=uuid.uuid4().hex)
    logger.debug("Offically started two episodes.")


@pytest.mark.dependency(name="init_gym", depends=["start_server"])
@pytest.mark.asyncio
async def test_async_gym():
    assert check_server(), "Server hasn't started"
    env = gym.make("CartPole-v0")
    aenv = agym.AsyncGym(env)
    obs = await aenv.reset()
    assert obs is not None
    assert isinstance(obs, np.ndarray)


@pytest.mark.dependency(name="single_step", depends=["init_gym", "new_episode"])
@pytest.mark.asyncio
async def test_rl_single_step():
    assert check_server(), "We weren't able to make a single step inside of the step function."
    async_client = aclient.AsyncPolicyClient(
        host="localhost",
        port=9900,
        secure=False,
    )
    episode_id = uuid.uuid4().hex
    env = gym.make("CartPole-v0")
    aenv = agym.AsyncGym(env)
    obs1 = await aenv.reset()

    await async_client.start_episode(episode_id=episode_id)
    action = await async_client.get_action(episode_id, obs1)
    obs2, reward, done, info = await aenv.step(action)
    await async_client.log_returns(episode_id, reward, info=info)
    await async_client.end_episode(episode_id, obs2)

    assert obs2 is not None, "Observation Doesn't Exist"
    assert obs1 is not None, "Observation Doesn't Exist"


    assert (not (obs1 == obs2).all()), "The observations are the exact same for some reason."


@pytest.mark.dependency(depends=["single_step"])
@pytest.mark.asyncio
async def test_close_server(xprocess):
    logger.info("Closing server")
    assert check_server(), "The server isn't up."

    try:
        xprocess.getinfo("rl_server").terminate()
    except Exception as e:
        logger.exception(e)
    is_closed = (not check_server())
    assert is_closed, "The server is still up when it should be closed."