import os
from jamboree import Jamboree

from see137.importers.testing.asyncrl import *
from see137.reinforcement import aclient, agym

REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = int(os.getenv("REDIS_PORT", "6379"))

RL_HOST = os.getenv("RL_HOST", "localhost")
RL_PORT = int(os.getenv("RL_PORT", "9900"))


@pytest.fixture(scope="session")
def jam_proc():
    return Jamboree(REDIS_HOST=REDIS_HOST, REDIS_PORT=REDIS_PORT)


@pytest.fixture(scope="session")
def server_path():
    parent_path = Path(__file__)
    _server_path = parent_path.parent.parent / "server.py"
    return _server_path


@pytest.fixture(scope="session")
def async_gym():
    env = gym.make("CartPole-v0")
    return agym.AsyncGym(env)


@pytest.fixture(scope="function")
def async_client():
    client = aclient.AsyncPolicyClient(host=RL_HOST, port=RL_PORT)
    return client