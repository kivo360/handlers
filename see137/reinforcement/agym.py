class AsyncGym:

    def __init__(self, env) -> None:
        self.env = env

    async def reset(self):
        return self.env.reset()

    async def step(self, action):
        obs, reward, done, info = self.env.step(action)
        return obs, reward, done, info

    async def action_sample(self):
        return self.env.action_space.sample()