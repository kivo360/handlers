import asyncio
import sys

from loguru import logger

logger.configure(
    handlers=[
        dict(sink=sys.stdout, colorize=True, enqueue=True),
    ]
)


async def run(cmd):
    proc = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )

    stdout, stderr = await proc.communicate()

    logger.info(f'[{cmd!r} exited with {proc.returncode}]')
    if stdout:
        logger.success(f'\n[stdout]\n\n{stdout.decode()}')
    if stderr:
        logger.error(f'\n[stderr]\n\n{stderr.decode()}')
