import cProfile
import pstats
from functools import wraps
import pandas as pd
import numpy as np
from creme import utils


def profile(
    output_file=None,
    sort_by='cumulative',
    lines_to_print=None,
    strip_dirs=False
):
    """A time profiler decorator.
    Inspired by and modified the profile decorator of Giampaolo Rodola:
    http://code.activestate.com/recipes/577817-profile-decorator/
    Args:
        output_file: str or None. Default is None
            Path of the output file. If only name of the file is given, it's
            saved in the current directory.
            If it's None, the name of the decorated function is used.
        sort_by: str or SortKey enum or tuple/list of str/SortKey enum
            Sorting criteria for the Stats object.
            For a list of valid string and SortKey refer to:
            https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats
        lines_to_print: int or None
            Number of lines to print. Default (None) is for all the lines.
            This is useful in reducing the size of the printout, especially
            that sorting by 'cumulative', the time consuming operations
            are printed toward the top of the file.
        strip_dirs: bool
            Whether to remove the leading path info from file names.
            This is also useful in reducing the size of the printout
    Returns:
        Profile of the decorated function
    """

    def inner(func):

        @wraps(func)
        def wrapper(*args, **kwargs):
            _output_file = output_file or func.__name__ + '.prof'
            pr = cProfile.Profile()
            pr.enable()
            retval = func(*args, **kwargs)
            pr.disable()
            pr.dump_stats(_output_file)

            with open(_output_file, 'w') as f:
                ps = pstats.Stats(pr, stream=f)
                if strip_dirs:
                    ps.strip_dirs()
                if isinstance(sort_by, (tuple, list)):
                    ps.sort_stats(*sort_by)
                else:
                    ps.sort_stats(sort_by)
                ps.print_stats(lines_to_print)
            return retval

        return wrapper

    return inner


def convert_pd_to_np(*args):
    output = [
        obj.to_numpy() if hasattr(obj, "to_numpy") else obj for obj in args
    ]
    return output if len(output) > 1 else output[0]


def sarsa_split(
    frame: pd.DataFrame,
    state_name="state",
    action_name="action",
    reward_name="reward"
):
    """Split the sarsa"""
    X_s = []
    Y_s = []
    for row in frame.iterrows():
        _states = np.array(row[1][state_name]).reshape(-1)
        _actions = np.array(row[1][action_name]).reshape(-1)
        _rewards = np.array(row[1][reward_name])
        X_s.append(np.hstack((_states, _actions)))
        Y_s.append(_rewards)

    return np.array(X_s), np.array(Y_s)


def loss_split(
    frame: pd.DataFrame,
    y_name="reward",
    y_hat_name="reward_hat",
):
    _y = frame[y_name].to_numpy()
    _y_hat = frame[y_hat_name].to_numpy()
    return _y, _y_hat


def sa_split(
    frame: pd.DataFrame,
    state_name: str = "state",
    action_name: str = "action",
):
    """ Get a state action pair the RL agent. Use to get stacked sequence of decisions and their associated states."""
    X_s = []
    for row in frame.iterrows():
        _states = np.array(row[1][state_name]).reshape(-1)
        _actions = np.array(row[1][action_name]).reshape(-1)
        X_s.append(np.hstack((_states, _actions)))
    return np.array(X_s)