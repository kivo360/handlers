from .utils.parameters import ModelParameters, is_acceptable_type
from .utils.parameters import convert_to_prices, convert_to_returns, brownian_motion_log_returns
from .main import generate_super_price