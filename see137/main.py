import copy
import multiprocessing
import random
import uuid
from concurrent.futures import ThreadPoolExecutor
from typing import Optional

import maya
import numpy as np
from jamboree.utils.context import timecontext
from loguru import logger

from see137 import SimulationCreator
from see137.components.acting.allocs import ActorManager
from see137.utils.trades.trade import DynamicTrade, TradeType

logger.disable("jamboree")
logger.disable("see137.handlers.exchange")
logger.disable("see137.handlers.allocation")
logger.disable("see137.utils.ordering")
logger.disable('darwin_ml')
# logger.disable('see137.handlers.states.envs.priv')

CORE_NUM = multiprocessing.cpu_count()


def main(max_runs: int = -1, episode_count: int = 1):
    simulation_creator = SimulationCreator(length=2000, ecount=episode_count)
    simulation_creator.start()

    # Get all of the active assets in the simulation
    current_step = 0
    while True:

        is_done = False
        for episode in simulation_creator.episodes:
            simulation_creator.current_episode = episode
            current_portfolio = simulation_creator.portfolio
            current_requirements = simulation_creator.requirements.unique()
            active_assets = current_requirements.assets
            asset_count = len(active_assets)
            current_active = copy.copy(active_assets)
            start_time = maya.now()._epoch
            while True:
                if not current_portfolio.pricing.is_next:
                    is_done = True
                    break

                active = current_active.pop()
                symbol = str(active.get('name'))
                # current_item = simulation_creator.pricing
                # current_item.name = symbol
                # current_item.episode = episode
                # current_item.live = False
                # current_item.reset()
                # current_item.closest_head()

                current_portfolio.add_asset(symbol)

                user_id = current_portfolio.user_id

                _subcats = simulation_creator.subcategories
                _subcats.pop("episode", None)

                statespace = simulation_creator.state
                statespace.name = symbol
                statespace.abbreviation = simulation_creator.abbreviation
                statespace.category = simulation_creator.category
                statespace.subcategories = _subcats
                statespace.user_id = user_id
                statespace.episode = episode
                statespace.asset_dict = active
                statespace.live = False
                statespace.is_mock = True
                statespace.is_mock_portfolio = True
                statespace.reset()
                statespace.step_log_pred(random.uniform(0, 4))
                statespace.step_log_returns(random.uniform(0, 5))
                statespace.save_action(np.random.uniform((1, 2)))
                results = statespace.step()
                logger.info(results)

                statespace.step_log_returns(random.uniform(0, 100))
                simulation_creator.current_episode = episode
                if current_portfolio.is_user_asset(active):
                    if random.uniform(0, 1) < 0.9:

                        trade_type: Optional[TradeType] = None
                        pct_transaction: float = 0.0
                        if random.uniform(0, 1) < 0.8:
                            trade_type = TradeType.LIMIT_BUY
                            pct_transaction = random.normalvariate(0.5, 0.08)
                        else:
                            trade_type = TradeType.LIMIT_SELL
                            pct_transaction = random.normalvariate(0.2, 0.06)
                        # Makes trade for portfolio
                        dynamic_trade = DynamicTrade(symbol, trade_type)
                        dynamic_trade.percentage = pct_transaction
                        current_portfolio.step(dynamic_trade)
                        logger.warning(dynamic_trade)
                    else:
                        logger.critical("Just doing a normal update")
                        current_portfolio.step()
                current_requirements.report_cardinality(active)
                if current_requirements.is_valid_cardinality:
                    current_requirements.flush_cardinality()
                    current_portfolio.time.step()
                    end_time = maya.now()._epoch
                    delta = (end_time - start_time)
                    per_asset = delta / asset_count * 1000
                    logger.info(
                        f"Next time step in episode. Took {delta}s for {asset_count} assets. Or {per_asset}ms per asset"
                    )
                    # This should be done elsewhere to prevent locking
                    current_portfolio.step_price(forced=True)
                    break

        if is_done is True:
            # TODO: Pop from group
            break

        if max_runs != -1:
            current_step += 1
            if current_step >= max_runs:
                break


if __name__ == "__main__":
    import ray
    ray.init()
    try:
        actors = ActorManager()
        actors.init_actors()

        processes = [
            multiprocessing.Process(target=main) for x in range((CORE_NUM))
        ]
        for process in processes:
            process.start()
        # main()
    except Exception:
        ray.shutdown()
