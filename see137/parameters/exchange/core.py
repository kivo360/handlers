from loguru import logger
from see137.parameters.exchange import base
from see137.utils.trades.trade import DynamicTrade, Trade, TradeType




class UpdateableExchangeState(base.CurrentExchangeState):
    
    def current_price(self, symbol:str="BTC") -> float:
        return self.prices.get_price(symbol)
    
    def current_amount(self, symbol:str="BTC") -> float:
        return self.portfolio.current_amount(symbol)

    def current_allocation(self, symbol:str) -> float:
        return self.allocation.current_allocation(symbol)
    
    def add_portfolio(self, symbol:str, amount:float):
        _current = self.current_amount(symbol)
        _current += amount
        self.portfolio.set_asset(symbol, _current)
        
    
    def add_balance(self, amount:float):
        balance = self.balance
        balance += amount
        self.balance = balance
    
    
    def make_trade(self, trade: Trade):
        trans_amt:float = trade.amount
        trans_total:float = trans_amt * trade.price
        if trade.is_buy:
            buy_tot:float = trans_total * -1
            buy_amt:float = trans_amt
            self.add_balance(buy_tot)
            self.add_portfolio(trade.symbol, buy_amt)
        elif trade.is_sell:
            sell_tot:float = trans_total
            sell_amt:float = (trans_amt * -1)
            self.add_balance(sell_tot)
            self.add_portfolio(trade.symbol, sell_amt)
    
    
    def update(self):
        pass



if __name__ == "__main__":
    example_prices = {
        'USD_ATL': 3108.3523027851,
        'USD_BCH': 777.7504649913,
        'USD_BTC': 2081.0927378499,
        'USD_EOS': 1976.29664766,
        'USD_ETH': 0.7516694972,
        'USD_XTZ': 0.3877544644
    }
    example_allocation = {
        'USD_ADA': 0.0, 'USD_ATL': 0.0,
        'USD_BCH': 0.0, 'USD_BTC': 0.0,
        'USD_EOS': 0.20528, 'USD_ETH': 0.44108,
        'USD_LINK': 0.0, 'USD_TRX': 0.16435,
        'USD_XRP': 0.12736, 'USD_XTZ': 0.06193
    }
    example_dict = {
        'exchange': 'fake_exchange',
        'user_id': '5efdd4b65e3540e8a0cea6a1632c6727',
        'episode': '3c5b2b6e786044b283f58a9db30d0fca',
        'live': False, 'type': 'portfolio', 'mtype': 'event', 'detail': 'all_spark', 'session': '3a6eec32b83e40baa860bf0468ffc02c',
        'portfolio': {'USD': 10000.0},
        'balance': 10000.0,
        'holdings': {'USD': 10000.0},
        'performance': {'balance': 10000.0, 'net_worth': 10000.0},
        'allocation': example_allocation,
        'event_id': '756236eed0d44331ac6821bd3260f34d',
        'time': 1592748614.7489548,
        'timestamp': 1592748616.7560668
    }
    exe_state = UpdateableExchangeState()
    exe_state.reset(example_dict, example_prices)
    logger.warning(exe_state.extract())