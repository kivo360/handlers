import abc
import time as tt
import typing
from typing import Any, Dict, Union

from loguru import logger
from pydantic import BaseModel, Field, validator

class HoldingsState(BaseModel):
    holdings: Dict[str, float] = {}
    total: float = 0.0

    def set_asset(self, symbol: str, amount: float, price: float):
        is_amount = isinstance(amount, float)
        is_price = isinstance(price, float)
        is_symbol = isinstance(symbol, str)
        if is_symbol and is_price and is_amount:
            asset_total = price * amount
            self.total += asset_total
            self.holdings[symbol] = asset_total

    def set_asset_direct(self, symbol: str, amount: float):
        is_amount = isinstance(amount, float)
        is_symbol = isinstance(symbol, str)
        if is_symbol and is_amount:
            self.total += amount
            self.holdings[symbol] = amount

    def set_base(self, symbol: str, amount: float):
        self.total += amount
        self.holdings[symbol] = amount

    def reinitialize(self, assets: Dict[str, Union[int, float]]):
        if len(assets) == 0:
            return
        self.holdings = {}
        self.total = 0.0
        for symbol, amount in assets.items():
            self.set_asset_direct(symbol, amount)

    def current_holding(self, symbol: str) -> float:
        return self.holdings.get(symbol, 0.000000000000000000001)
