import abc
import time as tt
import typing
from typing import Any, Dict, Union

from loguru import logger
from pydantic import BaseModel, Field, validator
from see137.parameters.exchange import allocations, holding, portfolios, pricing as price, utils

class CurrentExchangeState(BaseModel, abc.ABC):
    type: str = "portfolio"
    portfolio: portfolios.PortfolioState = portfolios.PortfolioState()
    allocation: allocations.AllocationState = allocations.AllocationState()
    holdings: holding.HoldingsState = holding.HoldingsState()
    prices: price.PricesState = price.PricesState()
    balance: float = 0.0
    net_worth: float = 0.0
    time: typing.Optional[float] = None
    timestamp: float = Field(default_factory=tt.time)
    # performance: dict = {}

    @property
    def performance(self) -> Dict[str, float]:
        return {
            "balance": self.balance,
            "net_worth": self.net_worth
        }

    @property
    def is_prices(self) -> bool:
        return not self.prices.empty()

    def is_valid_prior(self, batch: dict) -> bool:
        required = [
            'allocation',
            'balance',
            'holdings',
            'performance',
            'portfolio'
        ]
        return all(x in batch for x in required)

    def update(self):
        # Updates the variables. Can only be done if the price information has been set.
        raise NotImplementedError(
            "Expected to be subclassed for use. Please ensure of that."
        )

    def set_prices(self, prices: Dict[str, float]):
        if isinstance(prices, dict):
            if utils.is_valid_input(prices):
                self.prices.reinitialize(prices)
                
    def set_prices_and_update(self, prices: Dict[str, float]):
        self.set_prices(prices)
        self.update()  # Updates the valuation of the system. Only

    def reset(self, batch: Dict[str, Dict[Any, Any]], prices: typing.Optional[Dict[str, float]] = None):
        """
            Reset

            Given a batch load of all of the information for a given exchange/asset, this command loads the prior state. It doesn't do any processing.
        """
        if self.is_valid_prior(batch):
            current_portfolio = batch.get("portfolio", {})
            current_holdings = batch.get("holdings", {})
            current_allocation = batch.get("allocation", {})
            current_performance = batch.get("performance", 0.0)
            current_balance = current_performance.get("balance", 0.0)
            current_net_worth = current_performance.get("net_worth", 0.0)

            self.net_worth = current_net_worth
            self.balance = current_balance
            self.allocation.reinitialize(current_allocation)
            self.holdings.reinitialize(current_holdings)
            self.portfolio.reinitialize(current_portfolio)

        if prices is not None:
            # Set the prices of the assets. If it's a dupe we skip it.
            self.set_prices_and_update(prices)
            

    def extract(self) -> Dict[str, Any]:
        exclude_keys = {
            'portfolio': {'base'},
            'allocation': {'total'},
            'holdings': {'total'},
            'prices': ...,
            # 'balance': ...,
            'networth': ...
        }

        current = self.dict(exclude=exclude_keys, exclude_none=True)
        current['portfolio'] = current['portfolio']['portfolio']
        current['allocation'] = current['allocation']['allocation']
        current['holdings'] = current['holdings']['holdings']
        current['performance'] = self.performance
        return current

__all__ = ['CurrentExchangeState']
if __name__ == "__main__":
    example_prices = {
        'USD_ATL': 3108.3523027851,
        'USD_BCH': 777.7504649913,
        'USD_BTC': 2081.0927378499,
        'USD_EOS': 1976.29664766,
        'USD_ETH': 0.7516694972,
        'USD_XTZ': 0.3877544644
    }
    example_allocation = {
        'USD_ADA': 0.0, 'USD_ATL': 0.0,
        'USD_BCH': 0.0, 'USD_BTC': 0.0,
        'USD_EOS': 0.20528, 'USD_ETH': 0.44108,
        'USD_LINK': 0.0, 'USD_TRX': 0.16435,
        'USD_XRP': 0.12736, 'USD_XTZ': 0.06193
    }
    example_dict = {
        'exchange': 'fake_exchange',
        'user_id': '5efdd4b65e3540e8a0cea6a1632c6727',
        'episode': '3c5b2b6e786044b283f58a9db30d0fca',
        'live': False, 'type': 'portfolio', 'mtype': 'event', 'detail': 'all_spark', 'session': '3a6eec32b83e40baa860bf0468ffc02c',
        'portfolio': {'USD': 10000.0},
        'balance': 10000.0,
        'holdings': {'USD': 10000.0},
        'performance': {'balance': 10000.0, 'net_worth': 10000.0},
        'allocation': example_allocation,
        'event_id': '756236eed0d44331ac6821bd3260f34d',
        'time': 1592748614.7489548,
        'timestamp': 1592748616.7560668
    }
    exe_state = CurrentExchangeState()
    exe_state.reset(example_dict, example_prices)
    logger.warning(exe_state.extract())

    # logger.debug(example_prices)
    # logger.debug(example_dict)
    # logger.success(exe_state.dict())
