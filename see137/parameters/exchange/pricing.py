import abc
import time as tt
import typing
from typing import Any, Dict, Union

from loguru import logger
from pydantic import BaseModel, Field, validator

class PricesState(BaseModel):
    prices: Dict[str, float] = {}

    def set_asset(self, symbol: str, price: Union[int, float]):
        # is_amount = isinstance(amount, float)
        # is_price = isinstance(price, float)
        is_symbol = isinstance(symbol, str)
        if is_symbol:
            self.prices[symbol] = price

    def reinitialize(self, assets: Dict[str, Union[int, float]]):
        if len(assets) == 0:
            return
        self.prices = {}
        for symbol, amount in assets.items():
            self.set_asset(symbol, amount)

    def get_price(self, symbol: str) -> float:
        return self.prices.get(symbol, 0.00000000000000000000000001)

    def empty(self) -> bool:
        return len(self.prices) == 0
