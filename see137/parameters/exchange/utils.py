import abc
import time as tt
import typing
from typing import Any, Dict, Union

from loguru import logger
from pydantic import BaseModel, Field, validator
def is_valid_price(x, y) -> bool:
    is_name = isinstance(x, str) and bool(x)
    is_price = (isinstance(y, float) or isinstance(y, int)) and y >= 0
    return is_name and is_price


def is_valid_input(possible_prices: Dict[str, Union[int, float]]) -> bool:
    """Is Valid Input

    Determines if the price input is validself.

    Returns
    -------
    bool
        Returns true if valid
    """
    is_loaded = bool(possible_prices)
    is_all_valid = all([is_valid_price(x[0], x[1]) for x in possible_prices.items()])
    
    return is_loaded and is_all_valid

