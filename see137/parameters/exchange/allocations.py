from typing import Dict, Union

from pydantic import BaseModel, Field

class AllocationState(BaseModel):
    allocation: Dict[str, float] = {}
    total: float = Field(default=0.0, le=1, gt=0.0)

    def set_asset(self, symbol: str, amount: Union[int, float]):
        """Sets a single asset

        Parameters
        ----------
        symbol : str
            Symbol we're trading on
        amount : Union[int, float]
            Amount of allocation we have of that asset out of 100%self.
        """
        if isinstance(symbol, str) and (isinstance(amount, float) or isinstance(amount, int)) and (0 < amount <= 1):
            self.total += amount
            self.allocation[symbol] = amount

    def set_all(self, assets: Dict[str, Union[int, float]]):
        """Sets all

        Sets all assets with no regard of what came before it.

        Parameters
        ----------
        assets : Dict[str, Union[int, float]]
            A dictionary of the assets percentages we intend to set.
        """
        if len(assets) == 0:
            return
        for symbol, amount in assets.items():
            self.set_asset(symbol, amount)

    def reinitialize(self, assets: Dict[str, Union[int, float]]):
        if len(assets) == 0:
            return
        self.allocation = {}
        self.total = 0.0
        for symbol, amount in assets.items():
            self.set_asset(symbol, amount)

    def current_allocation(self, symbol: str):
        return self.allocation.get(symbol, 0.000000000000000000001)


__all__ = ['AllocationState']