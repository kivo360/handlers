import abc
import time as tt
import typing
from typing import Any, Dict, Union

from loguru import logger
from pydantic import BaseModel, Field, validator

class PortfolioState(BaseModel):
    base: str = "USD"
    portfolio: Dict[str, Union[int, float]] = {}

    def reset(self, base: str, amount: float):
        """Reset

        Sets the base instrument amount. To begin the portfolio.

        Parameters
        ----------
        base : str
            The base instrument
        amount : float
            The amount of that base instrument.
        """

        self.base = base
        self.portfolio[base] = amount

    def set_asset(self, symbol: str, amount: Union[int, float]):
        if isinstance(symbol, str) and (isinstance(amount, float) or isinstance(amount, int)):
            self.portfolio[symbol] = float(amount)

    def set_all(self, assets: Dict[str, Union[int, float]]):
        if len(assets) == 0:
            return
        for symbol, amount in assets.items():
            self.set_asset(symbol, amount)

    def reinitialize(self, assets: Dict[str, Union[int, float]]):
        """Reinitialize

        Reinitializes all of the assets. Use during reset and step calls to set all of the appropriate priors.

        Parameters
        ----------
        assets : Dict[str, Union[int, float]]
            The assets that we're setting.
        """
        if len(assets) == 0:
            return
        self.portfolio = {}
        # self.total = 0.0
        for symbol, amount in assets.items():
            self.set_asset(symbol, amount)

    def current_amount(self, symbol: str) -> float:
        """Gets the current number of units of a stock.

        Gets a current number of symbols of your stock from the databaseself.

        Parameters
        ----------
        symbol : str
            The symbol we're to be

        Returns
        -------
        float
            The amount of a given asset within a portfolio. Doesn't default to zero to prevent division bugs.
        """
        return self.portfolio.get(symbol, 0.000000000000000000001)

    def is_base(self, symbol: str) -> bool:
        """Is Base

        Determines if the current symbol is the base.

        Parameters
        ----------
        symbol : str
            Symbol we're checking against

        Returns
        -------
        bool
            Is the symbol a base.
        """
        return symbol == self.base