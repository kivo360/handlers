from see137.parameters import Parameter
from jamboree.utils.core import consistent_hash

class UniverseParameter(Parameter):
    def __init__(self, universe:str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.universe = universe
        self.verify()

    def verify(self):
        if not isinstance(self.universe, str):
            raise TypeError("Universe must be a string")

    