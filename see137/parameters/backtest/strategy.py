from see137.parameters import Parameter
from jamboree.utils.core import consistent_hash

class StrategyParameter(Parameter):
    def __init__(self, strategy:str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.strategy = strategy
        self.verify()

    def verify(self):
        if not isinstance(self.strategy, str):
            raise TypeError("Strategy ID must be a string")