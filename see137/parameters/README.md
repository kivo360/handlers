# Parameter Classes To Better Manage The Structure

Use to configure everything regarding backtest and strategies. We do this for both better type checking as well as some greater degree of preprocessing prior to sending a parameter in. We'll likely revert to this entirely as the system grows.

Here's an example of the usage:

```py
from dataclasses import dataclass
# What parameters would look like before now
dict_parameters = {
    "param1": "val1",
    "param2": "val2",
}

@dataclass
class ExampleParameters:
    param1:str = "val1"
    param2:str = "val2"

    def preprocessing_example(self):
        return "Some Transformation"
```