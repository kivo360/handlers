# TODO: Come up with a better importation theme. They're currently all clashing with each other.
from loguru import logger, _defaults
from .handlers.exchange import PortfolioHandler
from .handlers.allocation import AllocationHandler
from .handlers.requirements import RequirementsHandler
from .common.creation import SimulationCreator
from .common.accessor import SingleAccessor

logger.configure(
    activation=[("jamboree", False)],
    levels=[dict(name="NEW", no=13, icon="🖖", color="<magenta>")],
)
