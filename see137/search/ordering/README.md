# Trade Management

Manages the trades for an exchange or set of exchanges. Search will allow us to:

1. Find unopen trades
2. Find open trades
3. Find trades by exchange, user, market, size, etc
4. Change the status of trades