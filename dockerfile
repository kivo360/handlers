FROM tiangolo/uvicorn-gunicorn:python3.7

LABEL maintainer="Kevin Hill <kah.kevin.hill@gmail.com>"

COPY . .

RUN pip install fastapi
RUN pip install -r requirements.txt
RUN pip install sklearn redis

ENV MONGODB=mongo-service
ENV REDISHOST=redis-service
COPY ./app /app