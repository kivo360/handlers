from see137.common.creation import SimulationCreator
import uuid
import pytest

from see137.handlers.detached.load_balancer import DetachedLoadBalancer
from see137.importers import *


async def arange(count):
    for i in range(count):
        yield (i)


@pytest.mark.asyncio
async def test_asyncio_exchange(
    detached_load_balancer: DetachedLoadBalancer,
    ray_start_regular,
    full_sim_creator: SimulationCreator
):
    """Test the asyncio pytest marker."""
    _salt: str = uuid.uuid4().hex
    _hash: str = uuid.uuid4().hex
    detached_load_balancer.set_keys(_salt, _hash)
    await detached_load_balancer.start()
    pprams = await full_sim_creator.async_porfolio_params()
    sparams = await full_sim_creator.async_statespace_params()
    rparams = await full_sim_creator.async_requirements_params()

    await detached_load_balancer.exchange_step(parameters=pprams)
    await detached_load_balancer.allocation_step(parameters=pprams)
    state_space = None

    async for _ in arange(10):
        state_space = await detached_load_balancer.state_space_step(
            parameters=sparams
        )
        await detached_load_balancer.step_log_action(
            action=np.random.uniform((1, 2)), parameters=sparams
        )
        await detached_load_balancer.step_log_returns(
            returns=random.uniform(0, 300), parameters=sparams
        )
        await full_sim_creator.async_time_step()

    assert state_space is not None, "The state space is empty"
    asset: dict = sparams.get('asset_dict', {})

    await detached_load_balancer.report(asset, rparams)
    is_next = await detached_load_balancer.is_next(rparams)
    assert is_next is False, "We shouldn't be ready to take a next step inside of an episode."

    await detached_load_balancer.flush(rparams)
