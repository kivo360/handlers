import uuid
import random
from loguru import logger
import pytest
import asyncio
import ray
from jamboree import Jamboree
from see137.handlers.detached_actors import DetachedActorHandler



@pytest.fixture(scope="session")
def proc():
    processor = Jamboree()
    return processor


@pytest.fixture(scope="session")
def det_actor():
    detached_actor = DetachedActorHandler()
    detached_actor.processor = Jamboree()
    return detached_actor


def test_count_exchange_actors(det_actor, ray_start_regular):
    """Exchange Actors

    Create exchange actors.
    
    1. Check that the count = 3 (number per core)
    2. Check that the actors still live
    3. Kill all of the actors
    4. Check that the actors are dead. (is_dead(_id:str))
    """

    @ray.remote
    class Actor:

        def __init__(self):
            self.counter = 0

        def get_count(self):
            return self.counter

        def inc(self):
            self.counter += 1

        def get_id(self):
            return ray.worker.global_worker.worker_id

    detached_actor = det_actor

    _salt: str = uuid.uuid4().hex
    _hash: str = uuid.uuid4().hex
    detached_actor.set_keys(_salt, _hash)

    assert _salt == detached_actor.saltkey, "The generated salts don't match."
    assert _hash == detached_actor.hashkey, "The generated hashes don't match."

    num_exchnage_actors = 3
    actor_ids = [uuid.uuid4().hex for _ in range(num_exchnage_actors)]
    actors = [Actor.options(name=x).remote() for x in actor_ids]
    for x in actor_ids:
        detached_actor.add_actor(x, "exchange")

    assert detached_actor.count_actors("exchange") == num_exchnage_actors, f"Number of exchange actors doesn't equal: {num_exchnage_actors}"
    actor_name = random.choice(actor_ids)
    older_actor = ray.util.named_actors._get_actor(actor_name)
    current_count = ray.get(older_actor.get_count.remote())
    logger.warning(f"The current count inside of the actor is {current_count}")
    assert current_count is not None, "Was not able to get actor number for some reason."
    assert current_count == 0, "Count should be zero (0) right now."
    older_actor.inc.remote()
    current_count = ray.get(older_actor.get_count.remote())
    assert current_count == 1, "Count should be zero (1) right now."

    curr = detached_actor.get_actor(actor_name)
    assert curr._actor_id.hex() == older_actor._actor_id.hex()


def test_entirely_new_actors(
    det_actor: DetachedActorHandler, ray_start_regular
):
    """Exchange Actors

    Create exchange actors.
    
    1. Check that the count = 3 (number per core)
    2. Check that the actors still live
    3. Kill all of the actors
    4. Check that the actors are dead.
    """

    class Actor:

        def __init__(self):
            self.counter = 0

        def get_count(self):
            return self.counter

        def inc(self):
            self.counter += 1

    detached_actor = det_actor

    _salt: str = uuid.uuid4().hex
    _hash: str = uuid.uuid4().hex
    detached_actor.set_keys(_salt, _hash)

    assert _salt == detached_actor.saltkey, "The generated salts don't match."
    assert _hash == detached_actor.hashkey, "The generated hashes don't match."

    num_exchnage_actors = 3
    actor_name = "generic_actors"
    for _ in range(num_exchnage_actors):
        detached_actor.new_actor(Actor, actor_name)
    detached_actor.choice_by_name(actor_name)
    assert detached_actor.count_actors(actor_name) == num_exchnage_actors, f"Number of exchange actors doesn't equal: {num_exchnage_actors}"
    detached_actor.kill_all()
