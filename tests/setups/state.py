
class StateSpaceSetup:
    def __init__(self,
                 episode: str = "5a317aec96a94395a4782e8537e789c6",
                 metatype: str = "statespace",
                 submetatype: str = 'temporary_price',
                 category: str = 'markets',
                 name: str = 'USD_BCH',
                 live: bool = False,
                 abbreviation: str = "GEN",
                 subcategory: dict = {'market': 'stock', 'country': 'US', 'sector': 'faaaaake'}):
        self.episode = episode
        self.subcategory = subcategory
        self.metatype = metatype
        self.submetatype = submetatype
        self.category = category
        self.name = name
        self.live = live
        self.abbreviation = "GEN"
