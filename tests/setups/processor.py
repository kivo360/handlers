from jamboree import Jamboree


def jam_processor():
    return Jamboree(REDIS_HOST="localhost", REDIS_PORT="6379")
