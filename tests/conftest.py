import asyncio
import os
import sys
from contextlib import contextmanager
from multiprocessing import Value

import orjson
import pytest
import ray
from jamboree import Jamboree

from see137 import data
from see137.handlers.detached.load_balancer import DetachedLoadBalancer
from see137.handlers.states import actions, envs, machine
from see137.importers import *
from see137 import SimulationCreator

REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = int(os.getenv("REDIS_PORT", "6379"))


@pytest.fixture(scope="session")
def jam_proc():
    return Jamboree(REDIS_HOST=REDIS_HOST, REDIS_PORT=REDIS_PORT)


def get_default_fixure_internal_config():
    internal_config = orjson.dumps({
        "initial_reconstruction_timeout_milliseconds": 200,
        "num_heartbeats_timeout": 10,
        "object_store_full_max_retries": 3,
        "object_store_full_initial_delay_ms": 100,
    })
    return internal_config


def get_default_fixture_ray_kwargs():
    internal_config = get_default_fixure_internal_config()
    ray_kwargs = {
        "num_cpus": 1,
        "object_store_memory": 78 * 1024 * 1024,
        "_internal_config": internal_config,
    }
    return ray_kwargs


@contextmanager
def _ray_start(**kwargs):
    init_kwargs = get_default_fixture_ray_kwargs()
    init_kwargs.update(kwargs)
    # Start the Ray processes.
    address_info = ray.init(**init_kwargs)
    yield address_info
    # The code after the yield will run as teardown code.
    ray.shutdown()


@pytest.fixture(scope='session')
def ray_start_regular(request):
    param = getattr(request, "param", {})
    with _ray_start(**param) as res:
        yield res


collect_ignore = []
if sys.version_info[:2] < (3, 6):
    collect_ignore.append("async_fixtures/test_async_gen_fixtures_36.py")
    collect_ignore.append("async_fixtures/test_nested_36.py")


@pytest.fixture
def dependent_fixture(event_loop):
    """A fixture dependent on the event_loop fixture, doing some cleanup."""
    counter = 0

    async def just_a_sleep():
        """Just sleep a little while."""
        nonlocal event_loop
        await asyncio.sleep(0.1)
        nonlocal counter
        counter += 1

    event_loop.run_until_complete(just_a_sleep())
    yield
    event_loop.run_until_complete(just_a_sleep())

    assert counter == 2


@pytest.fixture(scope="session")
def proc():
    processor = Jamboree()
    return processor


@pytest.fixture(scope="session")
def detached_load_balancer(jam_proc: Jamboree):
    detached_actor = DetachedLoadBalancer()
    detached_actor.processor = jam_proc

    _actions = machine.StateSpaceMachine()
    _actions.processor = jam_proc
    _actions.init_default()
    _actions.user_id = uuid.uuid4().hex

    episode_set = [_actions.episode]
    abbv = _actions.asset_dict.abbreviation
    episode_list = episode_set
    abbv_list = [abbv]

    price_gen = data.PriceGenerator()
    price_gen.episodes = episode_list
    price_gen.length = 2000
    price_gen.assets = abbv_list
    generated = price_gen.generate()

    pricing = generated.get(_actions.asset_dict.abbreviation, {})

    _actions.price.store_time_df(pricing, is_bar=True)
    _actions.reset_technical_analysis()

    params = {
        "user_id": _actions.user_id, "episode": _actions.episode
    }

    detached_actor.set_params(params)
    return detached_actor


@pytest.fixture(scope="session")
def stp_actions(jam_proc: Jamboree):
    """StateSpace Actions

    All of the primary state space actions. It's used to hold highly specific functions 
    we'll use to shift data around. To be used with the other components (like context and Envs).

    Parameters
    ----------
    jam_proc : Jamboree
        Jamboree fixture

    Returns
    -------
    actions.ActionsStateSpace
        ActionStateSpace
    """
    _actions = actions.ActionsStateSpace()
    _actions.processor = jam_proc
    _actions.init_default()
    return _actions


@pytest.fixture(scope="session")
def full_sim_creator():
    simulation_creator = SimulationCreator(ecount=1)
    simulation_creator.start()
    return simulation_creator
