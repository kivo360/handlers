<!-- # Jamboree Handlers

Jamboree handlers is our super library that exist over jamboree. It's entire goal is to manage all complex handlers for our system so we can successfully run a backtest and deployment system.

It includes:

1. Our portflio and allocation management
2. Statespace management for reinforcement learning
3. Specific stratgey deployment schemes
4. Specified Metric Management
5. Storing statistics for later browsing. -->
