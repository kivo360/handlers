In order to generate the training pipeline, one that will ultimately be used to run code in live, we'll need have handlers to move information in an appropriate way. 


This will be extremely complex. We'll be observing every asset, pulling features for them, then digesting them to create more features to eventually create a decision. The issue with that is managing the complexity. We have a few parts that need to be worked on.

1. Separating training, backtest and live.
   * Having clear a separation of each.
   * Having the time to think about this, there's a lot to it.
     * We need to collect data. 
     * We can't act on all data we collect.
       * To solve this we'll create response procedures for each data source we'll collect. Essentially we can create a hash that represents all of the responses we'll have for it upon collection.
       * If we're collecting price data, we can simply set an active response and a general path for price information to run through.
         * This is basically a whitelist combined with a reactive dag. 
     * We also can't use the same models between each run.
       * For training, the models will be entirely different between training and live.
         * If we have a model object, we can decide to define two models. One for training, the other for live.
         * We can define it using a service handler.


## Work Methodology

In order to complete the backtesting framework we're going to first create a training pipeline then work backwards. The general idea is that with a training framework we'll have all of the basics finished then we can worry about creating a functioning pipeline.


### **What Consititutes A Finished Training Pipeline?**

**A training pipeline core activities:**

1. Creating items:
   * These are items like the episode id, an empty portfolio, fake data (stochastic data), etc. We're going to go into these items later on.
2. Push a pull event for each currency
3. Preprocessing information and predicting
4. Creating a state-space
5. Pull state space and make decision
   1. What ever decision logic we have
6. Send order to portfolio
   1. We process that order inside of the portfolio
   2. Get the sizing of the portfolio
   3. Process it through the rest of the portfolio
7. Checking to see if all of the assets we're testing are complete before we update the allocations and stepping through time.
8. If so, step through time. Step through allocations.


<!-- 
1. How we'll work on this
   1. We create a training pipeline only and iterate.
      1. What consititutes a finished training pipeline?
         1. Core activities
            1. Create items that are used throughout.
            2. Push EpisodeEvent that pulls relavent information.
               1. Preprocess the information
               2. Create a feature and store it
               3. NOTE: As long as we can pull information, we can just randomly store features into a feature store. 
            3. Make predicions
               1. Pull information from before to make a prediction.
               2. Store prediction
            4. Pull features to make a decision
               1. Create a state-space first
                  1. Check to see if all steps for a given currency are complete before creating state-space
                  2. If state-space steps are done, create a state-space
                  3. Save state-space
               2. Actually make decision
                  1. Pull state space
                  2. Get decision
                  3. Send decision into location that we can match future outcomes to current state/space decision pairs.
                  4. Reward agent based on predictions
                  5. Send decision to portfolio (another agent)
            5. Process order
               1. Send order to portfolio
               2. Before actual backtest we'll probably add the order/broker work
               3. In non-live modes (`when live == False` and `episode=='live'`)
         2. Creation Event
            1. Create episode ids
            2. Create a fake portfolio
               1. A userid you'll be planting into the episode manager
            3. Create fake data
            4. Randomly pick a sizable number of items from list and append them into the portfolio's
            5. Create a requirements setting
               1. The requirements are all of the things we'll need to do to push onto the next stage.
            6. Create a monitoring item for:
               1. user-id
               2. episode
               3. live
            7. State space creation
            8. Register the episode with ray
            9. Send in all events 
   2. The training pipeline will help us test and try new ideas
      1. We'll work to make the pipeline live at that point. It'll be in steps:
         1. Creating a price abstract around the data and multi-data handler.
         2. Set a timer to download data.
            1. Download data from tiingo every minute.
            2. This will store data then check our whitelist to get processed on.
            3. We can almost create a whitelist by figuring out if anybody has this asset in their live portfolio.
         3. Send processing -->

# Data Search Notes

```py
```