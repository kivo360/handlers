# 10 Handlers That Need to Be Done To Finish Pipeline

1. Sessions Handler
2. StateSpaceHandler
    1. Components
        * Preprocessing
            * What we'll use to pull from separate data sources to store
        * Requirements
            * What we'll be using to determine if all key components for a given item has been completed.
        * Multi-Data
            * Where we'll be pulling the data from
        * Time
            * Keeping track of the time
    2. Actions
        * Get State Space by item
            * Get the last `n` items from the state space and return.
        * Create State Space By Item
            * Runs through preprocessing logic
            * Creates a general state space
            * Stores the state space at the given time
        * Store action by item
        * Store reward by item
        * Match State, Action, Reward by item
3. NotificationHandler
    * Notifies the user upon completion