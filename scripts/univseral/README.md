# Universe Management

Inside of this folder we're going to create a series of scripts to manage how we'd create universes of assets.


The experiments we're going to be running through are the following:

- [ ] Save datasets to a specified key using metadata
- [ ] Finding all datasets and their associated information
- [ ] Pulling all datasets into a large json we can print onto screen
- [ ] Creating a universe of assets (without anything into them)
- [ ] Adding and removing assets to a universe of assets
- [ ] Creating a simple class for all of the required commands (to be imported into an API)
- [ ] Using a test driven approach for that class
- [ ] Neptune
- [ ] Comet Haley