import maya
import yfinance as yf
from addict import Dict
from loguru import logger
from anycache import anycache

from jamboree import Jamboree
from jamboree.handlers import DataHandler, MultiDataManagement
from jamboree.handlers.abstracted.datasets.price import PriceData
"""
    Insert data into the system with a given set of metadata.
"""


@anycache(cachedir='/tmp/anycache.my')
def get_dataset_info(ds_str: str):
    # ds_str = " ".join(ds_list)
    ticker = yf.Ticker(ds_str)

    return ticker.info


@anycache(cachedir='/tmp/anyticker.my')
def get_ticker_data(ds_str: str):
    data = yf.download(ds_str).dropna()
    return data


def get_price_handler():
    processor = Jamboree()
    pricing = PriceData()
    pricing.processor = processor
    return pricing


# self['name'] = name
#         self['abbreviation'] = abbv
#         self['subcategories'] = {
#             "market": market,
#             "country": country,
#             "sector": sector,
#             "exchange": exchange,
#         }

all_stop_words = [
        'the', 'they', 'them',
        'their', 'theirs', 'themselves'
    ]
# def get_data_by
def detokinize_name(name:str):
    long_name = str(name.replace(",", " ")).replace(".", "")
    long_name = " ".join(long_name.split())
    text = ' '.join([word for word in long_name.split() if word.lower() not in all_stop_words])
    return text


def main():
    pricing = get_price_handler()
    datalist = [
        "MSFT", 'SQ', 'TTD', "AAPL", "GOOG", "FB", 'ATVI', 'SHOP', 'BYND',
        'NFLX', 'PINS'
    ]
    market = "stock"
    sector = "technology"
    country = "US"
    handlers_by_ticker = {}
    data_by_ticker = {}
    for dl in datalist:
        info = get_dataset_info(dl)
        df = get_ticker_data(dl)


        long_name = info['longName']
        pricing['name'] = long_name
        pricing['abbreviation'] = dl
        pricing['subcategories'] = {
            "market": market,
            "country": country,
            "sector": sector,
            "exchange": "Dow Jones"
        }
        pricing.reset()
        handlers_by_ticker[dl] = pricing.copy()
        data_by_ticker[dl] = df
        logger.success(dl)


    # For each ticker, iterate through the dataframe and add the data one piece at a time
    for ticker_name, ticker_data in data_by_ticker.items():
        handler = handlers_by_ticker.get(ticker_name)
        logger.error(ticker_name)
        logger.success(handler)
        for row_index, row in ticker_data.iterrows():
            _timestamp = row_index.timestamp()
            row_dict = row.to_dict()
            row_dict['time'] = _timestamp
            handler.add_now(row_dict, is_bar=True)
            logger.debug(handler.last(ar="relative"))


if __name__ == "__main__":
    main()