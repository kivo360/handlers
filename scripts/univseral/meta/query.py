import random
from pprint import pprint
from typing import Dict, List

from loguru import logger

from scripts.univseral.controller import DatasetCommander


dctrl = DatasetCommander()
def price_metadata() -> List[Dict]:
    """Price Metadata 

    Get all available metadata for pricing.

    Returns
    -------
    List[Dict]
        List of metadata dictionaries.
    """
    
    price_metadata = dctrl.AllPrice()
    return price_metadata

def get_multis():
    universe_metadata = dctrl.AllUniverses()
    return universe_metadata




def main():
    # Get all price information
    # dctrl.RemoveAllPrice()
    _price_metadata = price_metadata()
    filtered = list(map(lambda x: (x['name'], x['id']), _price_metadata))
    logger.success(filtered)
    assert isinstance(_price_metadata, list)
    assert len(_price_metadata) > 0, "No price metadata available."
    queried_price_handlers = {

    }
    for _filt in filtered:
        name = _filt[0]
        identity = _filt[1]

        queried_price_handlers[name] = dctrl.GetPriceHandler(identity)
    
    unis = get_multis()
    logger.success(queried_price_handlers)
    print()
    logger.success(unis)
    print()
    chosen = random.choice(unis)
    logger.critical(chosen)
    


if __name__ == "__main__":
    main()
